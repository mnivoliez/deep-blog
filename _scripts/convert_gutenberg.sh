#!/bin/bash

for f in $1/*.md
do
  filename="${f##*/}"
  if [ "$(head -n 1 $f)" = '+++' ]; then
    # remove first line ---
    sed -i '1d' $f
  fi
  # insert front matter delimiter
  sed -i '1 i +++' $f
  # replace date format
  sed -i "s/published_date: \"\(....-..-..\) \(..:..:..\).*\"$/date = \1T\2Z/" $f $f
  # replace layout
  sed -i 's/layout.*$/template = \"page.html\"/' $f $f
  # replace frontmatter delimiters
  sed -i 's/---/+++/' $f $f
  # change draft line
  sed -i 's/is_draft:/draft =/' $f $f
  # change extra data
  sed -i 's/data:/[extra]/' $f $f
  # remove lang, dsq_thread_id
  sed -i '/.*lang:.*/d' $f
  sed -i '/.*dsq_thread_id:.*/d' $f
  # change img_cover to cover
  sed -i 's/img_cover:/cover =/' $f $f
  # replace : by =
  sed -i 's/title:/title =/' $f $f
  sed -i 's/title = \([^"].*[^"]\)$/title = "\1"/' $f $f
  sed -i 's/author:/author =/' $f $f
  sed -i 's/comments:/comments =/' $f $f
  # convert line ending to unix
  awk 'BEGIN{RS="^$";ORS="";getline;gsub("\r","");print>ARGV[1]}' $f
done
