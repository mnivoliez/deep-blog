#!/bin/bash

for f in $1/*.md
do
  filename="${f##*/}"
  filedate="$( echo "$filename" | sed 's/\(20[0-9][0-9]-[0-9][0-9]-[0-9][0-9]\).*$/\1/')"
  post_date="$( LC_TIME='en_EN.UTF-8' date -u -d $filedate +'%d %b %Y %T %z')"
  # remove first line ---
  sed -i '1d' $f
  # replace date format
  sed -i "s/date:.*/date: $post_date/" $f $f
  # replace permalink with lang
  sed -i 's/permalink: \/\([a-z][a-z]\).*/lang: \1/' $f $f
  # insert comments = true
  sed -i '1 i\comments: true' $f
  # insert the extends: posts.liquid
  sed -i '1 i\extends: post.liquid' $f
  # convert line ending to unix
  awk 'BEGIN{RS="^$";ORS="";getline;gsub("\r","");print>ARGV[1]}' $f
done
