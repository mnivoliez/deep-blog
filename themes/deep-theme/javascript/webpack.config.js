const path = require('path');

module.exports = {
    entry: {
        deep_navbar: './src/deep_navbar.js',
        dep_search: './src/dep_search.js',
    },
    output: {
        filename: '[name].js',
        path: path.resolve('../static/js')
    },
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /node_modules/,
            use: ["babel-loader", "eslint-loader"]
        }]
    },
    resolve: {
        extensions: ['*', '.js']
    },
    stats: {
        colors: true
    },
    devtool: 'source-map',
    mode: 'production',
};
