+++
title = "Crazy Orbit"
template = "page.html"
weight = 20
[extra]
comments = true
+++
Crazy Orbit is a game made with Unity during the Global Game Jam 2018. I was one of the two programmer on this project.
<!-- more -->
![Crazy orbit in action](https://raw.githubusercontent.com/mnivoliez/GGS-2018-Magnet/master/screenshot.png)

