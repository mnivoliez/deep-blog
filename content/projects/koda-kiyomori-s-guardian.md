+++
title = "Koda, Kiyomori's guardian"
template = "page.html"
weight = 10
[extra]
comments = true
+++
Koda Kiyomori's Guardian is a game I am working on as programmer with a team of 17 people in the scope of our formation at Gamagora. The project is 3.5 months old and is Unity based.
<!-- more -->

{{ youtube(id="RNpgW7T4oU8", class="youtube") }}
