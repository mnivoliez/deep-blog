+++
title = "Boids"
template = "page.html"
weight = 30
[extra]
comments = true
+++
This project was realized with Rust in the scope of the complex system course at Gamagora. It is simple inmplementation of boids using GGEZ.
<!-- more -->

{{ youtube(id="PWygfnB7oVw", class="youtube") }}
