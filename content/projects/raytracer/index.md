+++
title = "Raytracer"
template = "page.html"
weight = 30
[extra]
comments = true
+++
This project was realized with Rust in the scope of the rendering course at Gamagora.
<!-- more -->
![Raytracer in action](./raytracer.png)
