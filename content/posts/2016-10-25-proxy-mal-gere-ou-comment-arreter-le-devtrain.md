+++
title = "Proxy mal géré ou comment arreter le devtrain!"
date = 2016-10-25T00:00:00Z
template = "page.html"
[extra]
comments = true
+++
<p style="text-align: justify;">Bonjour!</p>
<p style="text-align: justify;">Aujourd'hui, on va parler de proxy et pourquoi il est important de bien le gérer vis-à-vis de ses équipes de développement.</p>
<p style="text-align: justify;">Avant toutes choses,</p>

<blockquote>Qu'est-ce qu'un proxy?</blockquote>
<p style="text-align: justify;">Un proxy c'est un point par lequel transite votre trafic Internet. Par exemple, à Sanofi, il y a un proxy qui gère toutes les connexions de chaque utilisateur. Donc si un utilisateur veut voir des vidéos de chatons, cette connexion passera par le proxy. Cela permet, entre autres, de sécuriser le réseau en interdisant certains sites ou en filtrant des connexions entrantes, etc... N'étant pas expert de la chose je laisse l'explication à ce stade.</p>

<blockquote>Mais c'est super utile un truc pareil! Pourquoi il poserait problème?</blockquote>
<p style="text-align: justify;">Bonne question! Et nous pouvons apporter plusieurs éléments de réponse. Je me concentrerai surtout sur ceux tirés de mon expérience. Un proxy peut "<strong>bloquer</strong>" . C'est pratique quand c'est un site dangereux ou une connexion douteuse. Mais quand il s'agit de vos dépendances de projet? Je m'explique. Imaginons que votre projet nécessite absolument "chaton_rocking_lib". Sans celle-ci, le projet ne peut fonctionner. Ce serait tellement dommage si vous ne pouviez pas la récupérer parce qu’un "mur", ou tout autre obstacle, se met au milieu, non? Eh bien c'est quelque chose qui peut arriver. Et quand ça arrive, ça crée de la frustration. Imaginons maintenant que le problème ne se limite pas à une dépendance mais à plusieurs. Le projet est drastiquement ralenti. L'équipe de développement peut se retrouver dans une situation d'impuissance: elle n'a pas la main (souvent) sur le proxy et ne peut s'en remettre qu'à l'équipe réseau.</p>
<p style="text-align: justify;">C'est typiquement le genre d'histoire qui mène à l’échec d'un projet, le désintéressement d'une équipe et la perte monétaire (retard, augmentation du turn over, baisse de qualité, etc...).</p>
<p style="text-align: justify;">Il est donc aujourd'hui crucial pour une entreprise de prendre en compte non pas que la sécurité de son réseau mais aussi l'utilisation qu'il va en être faite.</p>
<p style="text-align: right;">-- Mathieu</p>
&nbsp;
