+++
title = "Create a game with Amethyst (part 1)"
date = 2017-01-11T00:00:00Z
template = "page.html"
[extra]
comments = true
+++
<p style="text-align: justify;">Hello!</p>
<p style="text-align: justify;">Today we're gonna talk about a game project I'm making with my beloved one.</p>

<h2 style="text-align: justify;">"A game? What game?"</h2>
<p style="text-align: justify;">Good question! Let me introduce it.</p>
<p style="text-align: justify;">The goal is to make a game 20 minutes long. The game will set up a character (player) who will face his remorse while being guided by his consciousness (off speaker). In order to pursuit his quest, the player will have to go through 4 level of 5 min long, each one of them concluded by a boss.</p>
<p style="text-align: justify;">The game will be 2d (like Mario) where only the keyboard will be use.</p>
<p style="text-align: justify;">The current select name is "<strong>Absolution</strong>" but I'm open to any ideas for another name. So, do not hesitate!</p>

<h2 style="text-align: justify;">"Why should you make a game?"</h2>
There are several reasons.
<p style="text-align: justify;">First, it's fun. It's complex, interesting and, if you are with one or more good partners, it can become a really good experience.</p>
<p style="text-align: justify;">It's also a personal challenge. I intent to make video game my work for a living in the future and, with that in mind, I would like enter a video game school. Having a game in my "book" could help me to enter in such a school. It's also a challenge regarding the used technologies.</p>

<h2 style="text-align: justify;">"By the way, what are the technologies?"</h2>
The game is develop in <a href="https://www.rust-lang.org">Rust</a> using the <a href="https://github.com/amethyst/amethyst">Amethyst</a> game engine. Why those choices? First of all, Rust, even if it's young, is a powerful and very promising functional language. It's a compiled language that offer performances equivalent to c++ like languages. Secondly, Amethyst is an engine which use <a href="http://entity-systems.wikidot.com/">ECS</a> paradigm by using <a href="https://github.com/slide-rs/specs">SPECS</a> library. For all the graphique part (sprite, drawing, etc) it's my partner's work. I only integrate her work in the game.
<h2 style="text-align: justify;">"Ok, we get it. But why speak about it in your blog?"</h2>
I'm speaking about it because it interest me. I propose you to follow this amazing trip that my beloved one and myself are going through and by the same, discover Rust and Amethyst.

When a new article on the subject is out, I will post the link down below.
<p style="text-align: right;">-- Mathieu</p>
