+++
title = "Manage your dotfiles"
date = 2017-01-25T00:00:00Z
template = "page.html"
[extra]
comments = true
+++
<p style="text-align: justify;">Hello!</p>
<p style="text-align: justify;">Ah! Joys of configuration. All developer under Linux or Mac knows them. Even more if you are console addict like me. Configuration of Tmux, Vim, Zsh.... etc.</p>

<h2 style="text-align: justify;">"OK, we know what are dotfiles. Get to the point?"</h2>
The problems with the dotfiles are making them follow you on different workstation, how version them, etc...
<h2 style="text-align: justify;">"Can't you do that with git?"</h2>
You can. And you should. So we make a <code class="EnlighterJSRAW" data-enlighter-language="shell">dotfiles</code> repository, we put our files in it and then it get .... boring. We have to link our files from our repository to our   <code class="EnlighterJSRAW" data-enlighter-language="shell">$HOME</code> folder. Some of us will make a script to make it automatically.
<h2 style="text-align: justify;">"OK, we get it, it's boring. Your solution?"</h2>
<p style="text-align: justify;">My solution is [dotbot](https://github.com/anishathalye/dotbot)! It's a tool which automate the linking of your files and more.</p>

<h2 style="text-align: justify;">"Looks great, how do we use it?"</h2>
<p style="text-align: justify;">To use [dotbot](https://github.com/anishathalye/dotbot):</p>

<ul style="text-align: justify;">
 	<li>first, create your repository:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">mkdir dotfiles &amp;&amp; git init</pre>
</li>
 	<li>add [dotbot](https://github.com/anishathalye/dotbot) as git sub module:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">git submodule add https://github.com/anishathalye/dotbot</pre>
</li>
 	<li>copy the installation file provided by [dotbot](https://github.com/anishathalye/dotbot):
<pre class="EnlighterJSRAW" data-enlighter-language="shell">cp dotbot/tools/git-submodule/install .</pre>
</li>
 	<li>create your installation configuration:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">touch install.conf.yaml</pre>
</li>
 	<li>fill it up with your favorite editor:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">vim install.conf.yaml</pre>
<pre class="EnlighterJSRAW" data-enlighter-language="ini">- clean:   
  - '~'  
  - '~/.ssh'   
  - '~/bin/'
- link:
  ~/.gitconfig: dotfiles/gitconfig
  ~/.local.vim: dotfiles/local.vim     
  ~/.SpaceVim: dotfiles/SpaceVim
  ~/.profile: dotfiles/profile</pre>
</li>
</ul>
<h2 style="text-align: justify;">"How do you fill it?"</h2>
<p style="text-align: justify;">It's pretty simple:</p>

<ul style="text-align: justify;">
 	<li><code class="EnlighterJSRAW" data-enlighter-language="null">link</code> will define which to which place will be link the desire file. The element looks like <code class="EnlighterJSRAW" data-enlighter-language="null">target_place: file
</code></li>
 	<li><code class="EnlighterJSRAW" data-enlighter-language="null">clean</code> will clean up dead link in target directory. The element looks like that <code class="EnlighterJSRAW" data-enlighter-language="null">folder_to_check</code></li>
</ul>
<p style="text-align: justify;">For more information on how to use it, check the [README](https://github.com/anishathalye/dotbot/blob/master/README.md) of dotbot.</p>
<p style="text-align: justify;">And I can't not show my dotfile, so check them [here](https://gitlab.deep-nope.me/mnivoliez/dotfiles).</p>
<p style="text-align: right;">-- Mathieu</p>
