+++
title = "Koda: the year of the Tanuki"
date = 2018-02-08T06:42:00Z
template = "page.html"
draft = false
[extra]
comments = true
+++
Hello everyone! I am sorry for the delay, but the begining of the year was kind of a rush!
You mays have noticed some change on the site, including its name.
<!-- more -->

But the main main topic here will be "Koda, Kiyomori’s Guardian !".

## "Ok, but what is it?"

*"Kiyomori’s Guardian !"* is a game where you control *Koda* the *Tanuki* (a forest spirit), who has to fight the corruption of his home, the forest of *Kiyomori*. It's a 3D plateformer realized with *Unity*.
We are a team of 15 people including graphist, game designer and programmers. We make it in the scope of a school project and we aim to present it at the *Gamagora GameShow* which is organized by our school.

![The Koda's team](/images/koda_team.gif)
*I am the blond dancing one in the middle*

We got to develop it until end of March and there is a *metric tonne* of task to carry out. I believe we are motivate enough to carry them out.
Feel free to visit our page on [Facebook](https://www.facebook.com/Koda-Kiyomoris-Guardian-Projet-Gamagora-2018-538811786477040/) or our [Twitter](https://twitter.com/Kodamagora).

In a few words, stay tuned I shall speak about it again soon.
In the mean time, do not hesitate to ask about the project, I cannot promise to answer to everything but I will do my best.

--Mathieu
