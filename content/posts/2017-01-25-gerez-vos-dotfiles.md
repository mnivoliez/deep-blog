+++
title = "Gérez vos dotfiles!"
date = 2017-01-25T00:00:00Z
template = "page.html"
[extra]
comments = true
+++
<p style="text-align: justify;">Bonjour!</p>
<p style="text-align: justify;">Ah! Les joies de la configurations. Tout développeur ou bidouilleur sous Linux ou Mac les connais. Encore plus si vous êtes un accroc de la console comme moi. La configuration de Tmux, Vim, Zsh.... etc.</p>

<h2 style="text-align: justify;">"OK, on sait ce que sont les dotfiles. Et donc?"</h2>
<p style="text-align: justify;">Le problème avec les dotfiles c'est comment les faire suivre sur différentes machines, comment les versionner, etc...</p>

<h2 style="text-align: justify;">"Ben un Git et c'est fini non?"</h2>
<p style="text-align: justify;">C'est ce qu'on est tenté de ce dire. Et c'est la bonne approche. Donc on fait un repos <code class="EnlighterJSRAW" data-enlighter-language="shell">dotfiles</code>, on met nos fichiers dedans et c'est là que ça devient ... chiant. On doit faire des liens symboliques entre chaque fichier du repos et leur place dans notre  <code class="EnlighterJSRAW" data-enlighter-language="shell">$HOME</code>. Les plus dégourdi d'entre nous auront fait un petit script qui vas bien.</p>

<h2 style="text-align: justify;">"OK, on a compris, c'est barbant. Ta solution?"</h2>
<p style="text-align: justify;">Ma solution c'est [dotbot](https://github.com/anishathalye/dotbot)! C'est un outil qui vous permet de faire presque tous ce que nous avons au dessus. Seul la gestion du repos est de votre ressort.</p>

<h2 style="text-align: justify;">"Ça a l'air bien, comment ça s'utilise?"</h2>
<p style="text-align: justify;">Pour utiliser [dotbot](https://github.com/anishathalye/dotbot):</p>

<ul style="text-align: justify;">
 	<li>créer votre repos:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">mkdir dotfiles &amp;&amp; git init</pre>
</li>
 	<li>ajoutez [dotbot](https://github.com/anishathalye/dotbot) en submodule git:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">git submodule add https://github.com/anishathalye/dotbot</pre>
</li>
 	<li>copiez le fichier d'installation fournit par [dotbot](https://github.com/anishathalye/dotbot):
<pre class="EnlighterJSRAW" data-enlighter-language="shell">cp dotbot/tools/git-submodule/install .</pre>
</li>
 	<li>créez votre configuration d'installation:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">touch install.conf.yaml</pre>
</li>
 	<li>remplissez votre configuration avec votre éditeur préférez:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">vim install.conf.yaml</pre>
<pre class="EnlighterJSRAW" data-enlighter-language="ini">- clean:   
  - '~'  
  - '~/.ssh'   
  - '~/bin/'
- link:
  ~/.gitconfig: dotfiles/gitconfig
  ~/.local.vim: dotfiles/local.vim     
  ~/.SpaceVim: dotfiles/SpaceVim
  ~/.profile: dotfiles/profile</pre>
</li>
</ul>
<h2 style="text-align: justify;">"Comment se remplit se fichier?"</h2>
<p style="text-align: justify;">La configuration est assez simple:</p>

<ul style="text-align: justify;">
 	<li><code class="EnlighterJSRAW" data-enlighter-language="null">link</code> permet de lier vos fichiers prend un tableau où chaque éléments et de la forme <code class="EnlighterJSRAW" data-enlighter-language="null">lien_symbolique: le_fichier_dans_votre_repos</code></li>
 	<li><code class="EnlighterJSRAW" data-enlighter-language="null">clean</code> permet de supprimer les liens symboliques mort et prend un tableau où chaque éléments et de la forme <code class="EnlighterJSRAW" data-enlighter-language="null">dossier_a_verifier</code></li>
</ul>
<p style="text-align: justify;">Pour plus d'informations, je vous laisser lire le [README](https://github.com/anishathalye/dotbot/blob/master/README.md) de dotbot.</p>
<p style="text-align: justify;">Et comme je ne peux pas parlez des dotfiles sans donner les miens, c'est par [ici](https://gitlab.deep-nope.me/mnivoliez/dotfiles).</p>
<p style="text-align: right;">-- Mathieu</p>
