+++
title = "Un terminal sexy <3"
date = 2017-01-10T00:00:00Z
template = "page.html"
[extra]
comments = true
+++
Salutations!
<p style="text-align: justify;">Aujourd'hui, on va parler sexy!</p>
<p style="text-align: justify;">Si comme moi vous utilisez beaucoup la console ("vim user je vous regarde"), je vous propose l'outil <a href="http://terminal.sexy/">terminal.sexy</a>, un outil qui vous permettra de générer des jeux de couleur pour votre terminal favori.</p>
<p style="text-align: justify;"><img class="alignnone size-full wp-image-292" src="https://blog.deep-nope.me/wp-content/uploads/2017/01/terminal_sexy.png" alt="" width="1919" height="901" /></p>
<p style="text-align: justify;">L'interface vous permettra de tester différente couleur et d'exporter vers la configuration de votre terminal préféré.</p>
<p style="text-align: justify;">Ici un exemple pour termite:</p>

<pre class="EnlighterJSRAW" data-enlighter-language="ini">[colors]

# special
foreground      = #f1ebeb
foreground_bold = #f1ebeb
cursor          = #f1ebeb
background      = #272822

# black
color0  = #48483e
color8  = #76715e

# red
color1  = #dc2566
color9  = #fa2772

# green
color2  = #8fc029
color10 = #a7e22e

# yellow
color3  = #d4c96e
color11 = #e7db75

# blue
color4  = #55bcce
color12 = #66d9ee

# magenta
color5  = #9358fe
color13 = #ae82ff

# cyan
color6  = #56b7a5
color14 = #66efd5

# white
color7  = #acada1
color15 = #cfd0c2
</pre>
<p style="text-align: right;"> -- Mathieu</p>
