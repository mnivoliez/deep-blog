+++
title = "Tell me a story I could code #2"
date = 2018-05-11T12:55:31Z
template = "page.html"
draft = false
[extra]
comments = true
+++
Hello everyone! It's us again, Gaetan and Mathieu!
This is the second episode of *Tell me a story I could code*.
At our surprise, the concept worked more than we hope, and you, YES YOU, have been very prolific!
<!-- more -->

## "What was proposed?"

In short, a movement, a dialog and the apparition of a dragon was proposed.
Here is how we have proceed.

First, we have now a dragon, which is still a character but different from a human.
Second, characters can speak to each other.
Third, characters can go to or come from location.

So, let's rename our `Character` structure to `Human`:
```rust
struct Human {
  name: String,
  money: i32,
}
```

and then create the `Dragon` struct:
```rust
struct Dragon {
  name: String,
}
```

Next step abstract the Character behaviour:
```rust
pub trait Character {
    fn new(name: &str) -> Self;

    fn says_to(&self, other: &Self, msg: &str);

    fn move_to(&self, location: &Self);

    fn get_name(&self) -> &str;

    fn run_away_from(&self, location: &Self);
}
```
But as it, the only `Location` a `Charater` can go to or run from is another `Charater`. So let say that we want `Character` able to do that with a `Localisable` struct.

```rust
pub trait Localizable {
    fn get_location(&self) -> String;
}
```

We can then adapt the `Character` struct like that to tell that some metho will use a type `T` that `impl`ements `Localizable`:

```rust
pub trait Character {
    fn new(name: &str) -> Self;

    fn says_to<T: Character>(&self, other: &T, msg: &str);

    fn move_to<T: Localizable>(&self, location: &T);

    fn get_name(&self) -> &str;

    fn run_away_from<T: Localizable>(&self, location: &T);
}
```

Then, we can implement those `trait`s to `Character`

```rust
#[derive(Debug, Clone)]
pub struct Human {
    pub name: String,
    pub money: i32,
}

impl Human {
    pub fn find(&mut self, item: Item) {
        match item {
            Item::Money(amount) => {
                self.money += amount;
                println!("Oh {} penny! \" said {}.\"", amount, self.name);
            }
        }
    }
}

impl Character for Human {
    fn new(name: &str) -> Self {
        println!("There was a man named {}.", name);
        Human {
            name: name.to_owned(),
            money: 0,
        }
    }
    // Character needs to get back to another
    fn move_to<T: Localizable>(&self, location: &T) {
        println!("{} move to {}", self.name, location.get_location());
    }

    fn says_to<T: Character>(&self, other: &T, msg: &str) {
        println!("\"{}\" said {} to {}", msg, self.name, other.get_name());
    }

    fn get_name(&self) -> &str {
        self.name.as_str()
    }

    fn run_away_from<T: Localizable>(&self, location: &T) {
        println!("{} run away from {}", self.name, location.get_location());
    }
}

impl Localizable for Human {
    fn get_location(&self) -> String {
        self.name.clone()
    }
}
```
and for `Dragon`

```rust
#[derive(Debug, Clone)]
pub struct Dragon {
    pub name: String,
}

impl Dragon {
    pub fn fly_toward<T: Localizable>(&self, location: &T) {
        println!(
            "{} the dragon fly toward {}",
            self.name,
            location.get_location()
        );
    }
}

impl Localizable for Dragon {
    fn get_location(&self) -> String {
        format!("{} the dragon", self.name)
    }
}

impl Character for Dragon {
    fn new(name: &str) -> Self {
        println!("There was a dragon named {}.", name);
        Dragon {
            name: name.to_owned(),
        }
    }
    fn move_to<T: Localizable>(&self, location: &T) {
        println!("{} move to {}", self.name, location.get_location());
    }

    fn says_to<T: Character>(&self, other: &T, msg: &str) {
        println!("\"{}\" said {} to {}", msg, self.name, other.get_name());
    }

    fn get_name(&self) -> &str {
        &self.name
    }

    fn run_away_from<T: Localizable>(&self, location: &T) {
        println!(
            "How that, {} the dragon is running away from {}",
            self.name,
            location.get_location()
        );
    }
}
```

Note that we specified that only `Human` can find `Item` and that only `Dragon` can fly, so far.

Also note that the name inside `Character` was before a `&'static str` (meaning a [`String` slice](https://doc.rust-lang.org/std/primitive.str.html) with a `static` lifetime) and have been change for a `String`. The reason for that it is easier to work with the `String` and do not require to care about the lifetime (the "scope" in which the variable lives) in case we have to return it in a fashion manner. Also, note that using `static` mean that the lifetime of the reference is the same as the application lifetime.

Now the output looks like:
```
Once upon a time...
There was a man named Bob.
Oh 1 penny! " said Bob."
There was a man named Joe.
Bob move to Joe
"Dude! Look what I found!" said Bob to Joe
"You should invest the penny in the Sheriff of Nottingham's Bank" said Joe to Bob
"But I already have invested money in the bank, I want to buy a horse!" said Bob to Joe
There was a dragon named Igorrr.
Igorrr the dragon fly toward Bob
Bob run away from Igorrr the dragon
"The shiny thing belongs to me" said Igorrr to Bob
"Never! Founders keepers, you Reptilebug!" said Bob to Igorrr
```

The full code is available [here](https://gitlab.deep-nope.me/mnivoliez/tell-me-story-i-could-code).

More on:
* [Slice](https://doc.rust-lang.org/book/second-edition/ch04-03-slices.html)
* [Trait](https://doc.rust-lang.org/book/second-edition/ch10-02-traits.html)
* [Lifetime](https://doc.rust-lang.org/book/second-edition/ch10-03-lifetime-syntax.html) 

Special thanks to:
* Julian Chan
* Jason C. McDonald
* Faizan Rabbani
* Valentin Silvestre
* Sauban
* Salim Mahboubi

from the [dev.to post discussion](https://dev.to/mnivoliez/tell-me-a-story-i-could-code-1ho7/comments) for adding element to the story.

Same as last time, we are counting on you for the next episode :p

--
Gaetan and Mathieu
