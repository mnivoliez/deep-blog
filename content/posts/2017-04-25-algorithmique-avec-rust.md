+++
title = "Algorithmique avec RUST"
date = 2017-04-25T00:00:00Z
template = "page.html"
[extra]
comments = true
+++
<p style="text-align: justify;">Bonjour, aujourd'hui on va discuter algorithmique!</p>
<!-- more -->

<h2 style="text-align: justify;">"Algorithmique? Kezako?"</h2>
<p style="text-align: justify;">Très bonne question! Définissons d'abord un algorithme. Selon <a href="https://fr.wikipedia.org/wiki/Algorithme">Wikipédia</a>:</p>

<blockquote>Un <b>algorithme</b> est une suite finie et non ambiguë d’opérations ou d'instructions permettant de résoudre un problème ou d'obtenir un résultat.</blockquote>
<p style="text-align: justify;">Pour donner un exemple concret, quand nous cuisinons nous suivons une recette avec différentes "étapes" ou "opérations" (mettre la farine, rajouter de l'eau, battre les blanc en neige, etc...) permettant de réaliser un plat (obtenir le résultat).</p>
<p style="text-align: justify;">Et donc toujours selon <a href="https://fr.wikipedia.org/wiki/Algorithmique">Wikipédia</a>:</p>

<blockquote>L'<b>algorithmique</b> est l'étude et la production de règles et techniques qui sont impliquées dans la définition et la conception d'<a title="Algorithme" href="https://fr.wikipedia.org/wiki/Algorithme">algorithmes</a>, c'est-à-dire de processus systématiques de résolution d'un problème permettant de décrire précisément des étapes pour résoudre un <a title="Problème algorithmique" href="https://fr.wikipedia.org/wiki/Probl%C3%A8me_algorithmique">problème algorithmique</a>.</blockquote>
<p style="text-align: justify;">Ou plus, simplement, il s'agit de concevoir et d'étudier des algorithmes. Cela signifie également à réfléchir à l'efficacité de l'algorithme, et par conséquent, à son coût.</p>

<h2 style="text-align: justify;">"Faut payer en plus?"</h2>
<p style="text-align: justify;">Oui, chaque algorithme se paie. Pas d'inquiétude, on ne va pas vous prendre de l'argent mais des ressources liées à l'exécution de l'algorithme.</p>
<p style="text-align: justify;">Pour continuer sur mon exemple de la cuisine, pour chaque opération (mettre la levure, remuer, etc...) nous mobilisons nos mains, un saladier, notre temps etc....</p>
<p style="text-align: justify;">L'idée est de pouvoir quantifier cette "complexité".</p>
<p style="text-align: justify;">Selon <a href="https://fr.wikipedia.org/wiki/Th%C3%A9orie_de_la_complexit%C3%A9_(informatique_th%C3%A9orique)">Wikipédia</a> (oui, encore):</p>

<blockquote>La <b>théorie de la complexité</b> est un domaine des <a title="Mathématiques" href="https://fr.wikipedia.org/wiki/Math%C3%A9matiques">mathématiques</a>, et plus précisément de l'<a title="Informatique théorique" href="https://fr.wikipedia.org/wiki/Informatique_th%C3%A9orique">informatique théorique</a>, qui étudie formellement la <i>quantité de ressources</i> (temps et/ou espace mémoire) nécessaire pour résoudre un <i><a title="Problème algorithmique" href="https://fr.wikipedia.org/wiki/Probl%C3%A8me_algorithmique">problème algorithmique</a></i> au moyen de l'exécution d'un <a title="Algorithme" href="https://fr.wikipedia.org/wiki/Algorithme">algorithme</a>. Il s'agit donc d'étudier la difficulté intrinsèque de problèmes, pour ensuite les organiser par <a title="Classe de complexité" href="https://fr.wikipedia.org/wiki/Classe_de_complexit%C3%A9">classes de complexité</a>.</blockquote>
<h2 style="text-align: justify;">"Ok, comment on compte cette complexité?"</h2>
<p style="text-align: justify;">Alors, d'abord il existe une notation spécifique: O(f(n)) qui se lit "grand O de f(n)" où f est une fonction mathématique de n.<span id="MathJax-Element-9-Frame" class="MathJax" tabindex="0" data-mathml="&lt;math xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot;&gt;&lt;mi&gt;O&lt;/mi&gt;&lt;mo stretchy=&quot;false&quot;&gt;(&lt;/mo&gt;&lt;mi&gt;n&lt;/mi&gt;&lt;mo stretchy=&quot;false&quot;&gt;)&lt;/mo&gt;&lt;/math&gt;"><span id="MathJax-Span-32" class="math"><span id="MathJax-Span-33" class="mrow"><span id="MathJax-Span-37" class="mo"></span></span></span></span></p>
<p style="text-align: justify;">Considérons maintenant l'algorithme suivant:</p>

<pre class="EnlighterJSRAW" data-enlighter-language="null">Entrée: tableau t de taille n
Pour i de 1 à n faire
  ecrire t[i] // on à une action ici
Fin pour</pre>
<p style="text-align: justify;">On a un tableau avec n éléments en entré. Pour chaque élément, nous réalisons une action. A la fin de cette algorithme on aura donc fait n actions. La complexité est O(n).</p>
<p style="text-align: justify;">Si on avait fait deux actions, le nombre d'actions serait de 2n. Toutefois, les constantes ne sont pas importante car relatives au facteur n.</p>
<p style="text-align: justify;">En revanche,</p>

<pre class="EnlighterJSRAW" data-enlighter-language="null">Entrée: tableau t de taille n
Pour i de 1 à n faire
  Pour j de 1 à n faire
    ecrire t[i] // on à une action ici
  Fin pour
Fin pour</pre>
<p style="text-align: justify;">Dans cette algorithme on fait de boucle imbriqué on a donc n action répété n fois d'où la complexité est de O(n<sup>2</sup>).</p>

<h2 style="text-align: justify;">"Ok, et le Rust dans tout ça?"</h2>
<p style="text-align: justify;">Ben on va juste écrire les algorithmes en Rust.</p>
<p style="text-align: justify;">Bon je me rends compte que cet article commence à être long donc je vous donne rendez-vous dans le prochain article pour l'étude du tri à bulle.</p>
<p style="text-align: right;">-- Mathieu</p>
