+++
title = "Paperwork, a self-hosted Evernote"
date = 2017-01-26T00:00:00Z
template = "page.html"
[extra]
comments = true
+++
<p style="text-align: justify;"><strong>Beware! This article aims to people who knows linux, command line, mysql, php and nginx.</strong></p>
<!-- more -->
<p style="text-align: justify;">Hello!</p>
<p style="text-align: justify;">Today we are going to talk about <a href="http://paperwork.rocks/">Paperwork</a>!</p>

<h2 style="text-align: justify;">"What is it?"</h2>
<p style="text-align: justify;"><a href="http://paperwork.rocks/">Paperwork</a> is a tool to manage notes. In other words, it is like <a href="https://evernote.com/">Evernote</a> but at home.</p>

<h2 style="text-align: justify;">"OK, How do we install it?"</h2>
<p style="text-align: justify;">To install <a href="http://paperwork.rocks/">Paperwork</a>, open a terminal and enter these commands while being <strong>root</strong>:</p>

<ol style="text-align: justify;">
 	<li>Install php and mysql:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">// ubuntu
apt-get update
apt-get install mysql-server php5-mysql  nginx php5-fpm curl wget git php5-cli php5-gd php5-mcrypt nodejs</pre>
</li>
 	<li>Make the initial configuration of mysql:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">/usr/bin/mysql_secure_installation</pre>
</li>
 	<li>Activate mcrypt in php by modifying <code class="EnlighterJSRAW" data-enlighter-language="null">/etc/php5/fpm/php.ini</code> and <code class="EnlighterJSRAW" data-enlighter-language="shell">/etc/php5/cli/php.ini</code>:
<pre class="EnlighterJSRAW" data-enlighter-language="ini">extension=mcrypt.so</pre>
</li>
 	<li>Install composer:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer</pre>
</li>
 	<li>Go to folder <code class="EnlighterJSRAW" data-enlighter-language="null">/var/www</code> where we are going to set up <a href="http://paperwork.rocks/">Paperwork</a>:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">cd /var/www/</pre>
</li>
 	<li>Clone <a href="http://paperwork.rocks/">Paperwork</a> and enter in it:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">git clone https://github.com/twostairs/paperwork.git
cd ./paperwork/frontend/</pre>
</li>
 	<li>log into <code class="EnlighterJSRAW" data-enlighter-language="shell">mysql</code> and add a new user user with a base for <a href="http://paperwork.rocks/">Paperwork</a>:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">mysql -u root -p</pre>
<pre class="EnlighterJSRAW" data-enlighter-language="sql">DROP DATABASE IF EXISTS paperwork;
CREATE DATABASE IF NOT EXISTS paperwork DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
GRANT ALL PRIVILEGES ON paperwork.* TO 'paperwork'@'localhost' IDENTIFIED BY 'paperwork' WITH GRANT OPTION;
FLUSH PRIVILEGES;
quit</pre>
</li>
 	<li>Copy default configuration in order to let <a href="http://paperwork.rocks/">Paperwork</a> connect to database:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">cp app/storage/config/default_database.json app/storage/config/database.json</pre>
</li>
 	<li>Use the script provided by <a href="http://paperwork.rocks/">Paperwork</a> to fill up the database:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">php artisan migrate</pre>
</li>
 	<li>Give www-data rights to the folder where <a href="http://paperwork.rocks/">Paperwork</a> is (beware, on some systems the user isn't www-data but http):
<pre class="EnlighterJSRAW" data-enlighter-language="null">chown www-[extra]www-data -R /var/www/</pre>
</li>
 	<li>Add nginx configuration:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">vim /etc/nginx/sites-available/paperwork.domain.com</pre>
<pre class="EnlighterJSRAW" data-enlighter-language="ini">server {
        listen   80;
        # listen 443 ssl;

        root /var/www/paperwork/frontend/public;
        index index.php index.html index.htm;

        server_name example.com;

        # server_name example.com;
        # ssl_certificate /etc/nginx/ssl/server.crt;
        # ssl_certificate_key /etc/nginx/ssl/server.key;

        location / {
                try_files $uri $uri/ /index.php;
        }

        error_page 404 /404.html;

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
              root /usr/share/nginx/www;
        }

        # pass the PHP scripts to FastCGI server listening on the php-fpm socket
        location ~ \.php$ {
                try_files $uri =404;
                fastcgi_pass unix:/var/run/php5-fpm.sock;
                fastcgi_index index.php;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                include fastcgi_params;

        }

}
</pre>
<pre class="EnlighterJSRAW" data-enlighter-language="shell">ln -s /etc/nginx/sites-avalaible/paperwork.domain.com /etc/nginx/sites-enable/paperwork.domain.com
</pre>
</li>
 	<li> Add a file <code class="EnlighterJSRAW" data-enlighter-language="shell">setup</code> to avoid <a href="http://paperwork.rocks/">Paperwork</a> configuration failure:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">sudo -u www-data touch app/storage/config/setup</pre>
</li>
 	<li> Install nodejs:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">curl -sL https://deb.nodesource.com/setup_7.x |  bash -
apt-get install -y nodejs
</pre>
</li>
 	<li>Install <code class="EnlighterJSRAW" data-enlighter-language="shell">bower</code> and <code class="EnlighterJSRAW" data-enlighter-language="shell">gulp</code> globally:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">npm install -g gulp bower</pre>
</li>
 	<li> Install <a href="http://paperwork.rocks/">Paperwork</a> dependencies:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">sudo -u www-data npm install
sudo -u www-data bower install
sudo -u www-data gulp</pre>
</li>
 	<li>Restart nginx et php:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">service nginx restart
service php5-fpm restart</pre>
</li>
</ol>
<p style="text-align: justify;">And voilà! You got <a href="http://paperwork.rocks/">Paperwork</a> running on port 80.</p>
<p style="text-align: right;">-- Mathieu</p>
