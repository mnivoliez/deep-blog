+++
title = "Savoir quand arrêter."
date = 2017-03-21T00:00:00Z
template = "page.html"
[extra]
comments = true
+++
Bonjour, aujourd'hui je vais faire un retour d'expérience sur Amethyst et pourquoi je l'ai abandonné pour le <a href="http://blog.deep-nope.me/fr/2016/11/29/creer-un-jeu-avec-amethyst-part-1/">jeu</a> que nous réalisons avec ma tendre et douce.
<!-- more -->
<h2>"Tu as abandonné Amethyst?"</h2>
Oui. Et pour comprendre, posons un peu de contexte. J'aime la programmation fonctionnel et le développement orienté données. De fait, Amethyst étant un moteur utilisant un ECS (Entities Components Systems) correspondait à ma vision des choses. Le moteur est neuf, la communauté est active....
<h2>"Mais????"</h2>
Eh bien il a plusieurs problèmes! D'abord il est neuf, très neuf: il y a des changements d'api si régulièrement qu'il est nécessaire de ré-écrire une partie du code fréquemment. Ensuite il est plus orienté 3d alors que nous développons un jeu 2d. Enfin bref, c’était devenue compliqué de développer avec.
<h2>"Tu aurais pu le voir depuis le début non?"</h2>
Eh ben... Oui j'aurais pu, j'aurais dû! Mais j'étais si enthousiaste que j'ai préféré ne pas le voir. Et c'est tout le point de mon article: nous nous laissons souvent avoir par nos sentiments. Nous refusons une techno parce que nous la pensons mauvaise et nous nous accrochons à une autre car elle nous plaît. J'ai refusé de voir les défauts de développer avec Amethyst car j'aime "l'idée de cette techno" et cela à rendu le développement douloureux, fastidieux. D'où le changement. Je pense qu'il est de notre devoir d'accepter ces sentiments envers nos "outils", pas pour les rejeter systématiquement mais pour en être conscient et les maitrîser.

Voila, c'est mon retour. Votre avis?

&nbsp;
