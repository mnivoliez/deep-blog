+++
title = "Fait VS Mythe social"
date = 2016-12-13T00:00:00Z
template = "page.html"
[extra]
comments = true
+++
[embed]https://www.youtube.com/watch?v=rnu9e1ft5qw&t=0s[/embed]
<p style="text-align: justify;">Bonjour! Contrairement à d'habitude, l'article commence par une vidéo. Elle va nous servir tout au long de cet article afin de discuté de constructions sociales et de leurs dérives.</p>

<h3 style="text-align: justify;">"Mais, qu'est-ce qu'une construction sociale?"</h3>
<p style="text-align: justify;">C'est une bonne question! Une construction sociale, selon <a href="https://en.wikipedia.org/wiki/Social_constructionism">Wikipédia</a>, est</p>

<blockquote>
<p style="text-align: justify;">le sens, la notion ou la connotation placée sur un objet ou un évènement par la société, qui est acceptée par les habitants cette société [...] une construction sociale peut être une idée accepté largement par une communauté sans être nécessairement le reflet de la réalité.</p>
</blockquote>
<p style="text-align: justify;">En résumé, on pourrait dire qu'une construction sociale est une idée que la société dans son ensemble à adopté qu'elle se rapporte ou non à la réalité.</p>

<h3 style="text-align: justify;">"Mais dans le titre tu as parlé de Mythe social?"</h3>
<p style="text-align: justify;">Oui, en effet. J'ai choisi ce terme pour parler de constructions sociales qui ne se réfèrent pas à la réalité. Revenons à la base, qu'est-ce qu'un fait et qu'est-ce qu'un mythe? Un fait est un évènement qui s'est déroulé selon Wikipédia. Je rajouterai qu'un fait est mesurable. "J'ai mangé trois cookies" est un fait. "Je suis le plus grand fan de Trump" n'est pas un fait, je ne peux pas le mesurer et donc pas le prouver. En revanche, un mythe est une construction imaginaire amenant à la croyance. Selon <a href="https://fr.wikipedia.org/wiki/Mythe">Wikipédia</a> (et oui encore):</p>

<blockquote>Un mythe est une construction imaginaire (récit, représentation, idées) qui se veut explicative de phénomènes cosmiques ou sociaux et surtout fondatrice d'une pratique sociale en fonction des valeurs fondamentales d'une communauté à la recherche de sa cohésion.</blockquote>
<h3 style="text-align: justify;">"Mais du coup, un mythe social, qu'est ce que ça pourrait être?"</h3>
<p style="text-align: justify;">Eh bien, ça pourrait être une croyance populaire qui se révèle fausse, comme celle présenté dans la vidéo au-dessus.</p>
<p style="text-align: justify;">Le problème des mythes populaires, c'est qu'ils induisent en erreur tout en rendant difficile l'accès à des faits réels. Par exemple le fait qu' "ils touchent plus avec le RSA que nous en travaillant". Où est la preuve? Le fait? Le plus souvent nous avons affaire à une croyance plus qu'à un réel relevé. Et par de ce fait le débat n’apparaît pas, car il s'agit d'une foi en une chose qui n'est pas un fait avéré.</p>
<p style="text-align: justify;">Si je dis tout ça c'est pour amener à la remise en question de nos croyances. Pour amener à la recherche des faits et uniquement de ceux-ci.</p>
<p style="text-align: right;">-- Mathieu</p>
