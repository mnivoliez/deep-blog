+++
title = "Débuter avec Rust : Présentation et installation"
date = 2017-09-09T00:00:00Z
template = "page.html"
[extra]
comments = true
+++
<p style="text-align: justify;">Bonjour, aujourd'hui on va parler de Rust.</p>
<!-- more -->

<h2 style="text-align: justify;">"Mais tu en parles souvent non?"</h2>
<p style="text-align: justify;">Oui, raison de plus pour correctement vous introduire ce langage qui m'a séduit.</p>

<h2 style="text-align: justify;">"OK, on t'écoute"</h2>
<p style="text-align: justify;">Commençons par le commencement. Rust est un langage de programmation compilé. C'est-à-dire que le code que vous écrivez sera traduit vers un langage que l'ordinateur peut comprendre. Cette traduction sera l'<strong>exécutable</strong>. Rust est également un langage fortement typé. Autrement dit, les variables (nous y reviendrons) sont d'un certain type. Enfin, Rust est présenté sur son site <a href="https://www.rust-lang.org/fr-FR/">officiel</a> par:</p>

<blockquote>Rust un langage de programmation système ultra-rapide, qui prévient les erreurs de segmentation et garantit la sûreté entre threads</blockquote>
<h2 style="text-align: justify;">"Erreur de segmentation? Sûreté entre les threads?"</h2>
<p style="text-align: justify;">Si vous n'avez jamais pratiqué un langage dit "bas-niveau" tel que le <strong>C</strong> ou le <strong>C++</strong>, ces termes vous sont certainement nouveaux. Je vais les définir rapidement, mais nous y reviendrons une autre fois.</p>
<p style="text-align: justify;">Une erreur de segmentation c'est quand votre programme tente d'accéder à de la mémoire qui ne lui pas été allouée. C’est comme si l’on vous donnait un bureau sur lequel travailler, et que vous vous étalé sur celui de votre collègue. Votre collègue risque de s'énerver, et dans le cas de votre programme, il va s’arrêter brutalement.</p>
<p style="text-align: justify;">Pour les threads, il s'agit de "processus légers" qui peuvent fonctionner en parallèle. Imaginez un tas de vaisselle à faire, vous pouvez utiliser plusieurs personnes pour la faire. Le danger réside dans le cas où deux personnes tenteraient de laver la même assiette en même temps.</p>
<p style="text-align: justify;">Rust dit prévenir ces deux risques. Au cours de cette série, nous aurons l'occasion de montrer comment.</p>

<h2 style="text-align: justify;">"OK, mais c'est encore un peu flou..."</h2>
<p style="text-align: justify;">C'est normal. Mais on y reviendra plus tard. C'est des notions compliquées que vous viendrez à comprendre avec la pratique.</p>

<h2 style="text-align: justify;">"D'accord. On commence?"</h2>
<p style="text-align: justify;">Tout à fait. Et première chose à faire est d'installer Rust. Pour ce faire, nous allons utiliser l'utilitaire <a href="https://rustup.rs/">Rustup</a>. Vous devrez vous munir d'une console afin de taper les commandes.</p>
<p style="text-align: justify;">Si vous êtes sur Linux ou Mac, la commande suivante devrait suffire:</p>

<pre class="EnlighterJSRAW" data-enlighter-language="shell">curl https://sh.rustup.rs -sSf | sh</pre>
<p style="text-align: justify;">Si vous êtes sous Windows, rendez-vous sur le <a href="https://rustup.rs/">site de Rustup</a> pour télécharger l’exécutable, puis suivez les instructions.</p>
<p style="text-align: justify;">Ensuite, nous allons utiliser <strong>Cargo</strong>!</p>

<h2 style="text-align: justify;">"Cargo?"</h2>
<p style="text-align: justify;">C'est un outil de Rust. Placez-vous dans le dossier de votre choix:</p>
<p style="text-align: justify;">Pour Linux</p>

<pre class="EnlighterJSRAW" data-enlighter-language="shell">cd ~/workspace</pre>
<p style="text-align: justify;">Pour Windows</p>

<pre class="EnlighterJSRAW" data-enlighter-language="shell">cd %USERPROFILE%\projects</pre>
<p style="text-align: justify;">Et ensuite</p>

<pre class="EnlighterJSRAW" data-enlighter-language="shell">cargo new hello_world --bin
cd hello_world</pre>
<p style="text-align: justify;">Le <code class="EnlighterJSRAW" data-enlighter-language="null">--bin</code>permet de signifier à Cargo que nous allons faire un exécutable. Sans cette option, il créerait une librairie.</p>
<p style="text-align: justify;">Dans le dossier nouvellement créé, nous pouvons voir un dossier <code class="EnlighterJSRAW" data-enlighter-language="shell">src</code>qui contiendra nos sources, et un fichier <code class="EnlighterJSRAW" data-enlighter-language="shell">Cargo.toml</code>qui contient les informations relative à notre projet:</p>

<pre class="EnlighterJSRAW" data-enlighter-language="ini">[package]
name = "hello_world"
version = "0.1.0"
authors = ["Mathieu Nivoliez"]

[dependencies]
</pre>
<ul style="text-align: justify;">
 	<li>Le nom</li>
 	<li>La version</li>
 	<li>L'auteur</li>
 	<li>Les dépendances</li>
</ul>
<p style="text-align: justify;">Regardons maintenant le <code class="EnlighterJSRAW" data-enlighter-language="shell">main.rs</code>dans le dossier <code class="EnlighterJSRAW" data-enlighter-language="shell">src</code>:</p>

<pre class="EnlighterJSRAW" data-enlighter-language="rust">fn main() {
    println!("Hello, world!");
}
</pre>
<h2 style="text-align: justify;">"Et comment lit-on ça?"</h2>
<p style="text-align: justify;">De la manière suivante: nous avons une fonction <code class="EnlighterJSRAW" data-enlighter-language="rust">fn</code> nommée <code class="EnlighterJSRAW" data-enlighter-language="rust">main()</code>qui, lorsqu’appelée, exécutera la macro <code class="EnlighterJSRAW" data-enlighter-language="rust">println!</code>avec en paramètre la chaîne de caractères <code class="EnlighterJSRAW" data-enlighter-language="rust">"Hello, world!"</code>ce qui affichera donc le message "Hello, world!" lors de l'exécution.</p>
<p style="text-align: justify;">Nous pouvons lancer donc la commande <code class="EnlighterJSRAW" data-enlighter-language="shell">cargo run</code>afin d'afficher la chose dans la console.</p>
<p style="text-align: justify;">Nous reviendrons lors d'un prochain article sur les fonctions et macros.</p>
<p style="text-align: justify;">Pour le moment, nous avons installé Rust et créé un hello world.</p>
<p style="text-align: justify;">À bientôt, pour un prochain article sur comment débuter avec Rust.</p>
<p style="text-align: right;">-- Mathieu</p>
&nbsp;

Sources: <a href="https://doc.rust-lang.org/book/second-edition/ch01-00-introduction.html">Le livre Rust</a>
