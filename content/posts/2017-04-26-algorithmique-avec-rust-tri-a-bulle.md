+++
title = "Algorithmique avec Rust: Tri à bulle"
date = 2017-04-26T00:00:00Z
template = "page.html"
[extra]
comments = true
+++
<p style="text-align: justify;">Bonjour, aujourd'hui on va parler du tri à bulle.</p>
<!-- more -->

<h2 style="text-align: justify;">"Le tri à bulle?"</h2>
<p style="text-align: justify;">Le tri à bulle est un algorithme simple qui nous permettra de nous familiariser avec l'algorithmie en Rust et également qui nous permettra de comprendre ce que signifie la complexité algorithmique dont nous avons parlé dans le précédent article.</p>

<h2 style="text-align: justify;">"Ok, en quoi il consiste ce tri à bulle?"</h2>
<p style="text-align: justify;">Basiquement, cette algorithme compare deux à deux tous les éléments d'un tableau et les permute s'il sont mal trié.</p>
<p style="text-align: justify;">Donc nous avons un algorithme qui prend un tableau en entrée et donne un tableau en sortie.</p>

<pre class="EnlighterJSRAW" data-enlighter-language="rust">fn bubble_sort(vec_to_sort: &amp;Vec&lt;i32&gt;) -&gt; Vec&lt;i32&gt; {
    let mut result = vec_to_sort.clone();
    return result;
}</pre>
<p style="text-align: justify;">Nous définissons ici une fonction, avec le mot clé "fn", qui se nomme "bubble_sort". Elle prend en paramètre une variable appelée "vec_to_sort" qui est un vecteur de i32. Un vecteur est un tableau qui change de taille au court du temps et un i32 est un entier écrit sur 32 bit. Cette fonction retourne un autre vecteur de i32.</p>
<p style="text-align: justify;">L'algorithme compare tous les éléments deux à deux.</p>

<pre class="EnlighterJSRAW" data-enlighter-language="rust">fn bubble_sort(vec_to_sort: &amp;Vec&lt;i32&gt;) -&gt; Vec&lt;i32&gt; {
    let mut result = vec_to_sort.clone();
    for i in 0..result.len() {
        for y in 0..result.len() {
            if result[i] &lt; result[y] {
            }
        }
    }
    return result;
}</pre>
<p style="text-align: justify;">Puis, nous permutons les éléments s'ils sont mal triés.</p>

<pre class="EnlighterJSRAW" data-enlighter-language="rust">fn bubble_sort(vec_to_sort: &amp;Vec&lt;i32&gt;) -&gt; Vec&lt;i32&gt; {
    let mut result = vec_to_sort.clone();
    for i in 0..result.len() {
        for y in 0..result.len() {
            if result[i] &lt; result[y] {
                result.swap(i, y);
            }
        }
    }
    return result;
}</pre>
<p style="text-align: justify;">Vous pouvez tester ce code <a href="https://is.gd/OLZuLj">ici</a>.</p>
<p style="text-align: justify;">Comptons maintenant les actions.</p>

<ol style="text-align: justify;">
 	<li>La copie du tableau d'entrée est négligeable.</li>
 	<li>Nous bouclons ensuite sur tous les éléments: n actions.</li>
 	<li>Nous rebouclons ensuite sur tous les éléments: n actions.</li>
 	<li>On compare les éléments entre eux.</li>
 	<li>On échange, si besoins, les éléments.</li>
</ol>
<p style="text-align: justify;">On a donc dans le pire des cas n*n*1+1 actions. Or les constantes ne sont pas importantes donc nous avons une complexité en O(n<sup>2</sup>).</p>
<p style="text-align: justify;">Pour donner une idée de ce que cela représente, si nous avons un processeur capable d'exécuter 100 opérations par secondes et que nous avons un tableau de 100 éléments, cette algorithme mettra 1m40 à trier ce tableau.</p>

<h2 style="text-align: justify;">"Bon, ok pour l'algo... Et si on veux trier un tableau avec des chaînes de caractères? Ou même le trier du plus grand au plus petit?"</h2>
<p style="text-align: justify;">Bonne remarque! Pour cela, il faut que la fonction devienne générique et qu'elle fasse appelle aux <a href="https://doc.rust-lang.org/book/closures.html">closures</a> (des sortes de fonction anonyme).</p>
<p style="text-align: justify;">Nous pouvons ainsi modifier le code comme cela:</p>

<pre class="EnlighterJSRAW" data-enlighter-language="rust">fn bubble_sort&lt;T: std::clone::Clone, F&gt;(vec_to_sort: &amp;Vec&lt;T&gt;, compar : F) -&gt; Vec&lt;T&gt; 
where F : Fn(&amp;T,&amp;T) -&gt; bool {
    let mut result = vec_to_sort.clone();
    for i in 0..result.len() {
        for y in 0..result.len() {
            if compar(&amp;result[i], &amp;result[y]) {
                result.swap(i, y);
            }
        }
    }
    return result;
}</pre>
<p style="text-align: justify;">Basiquement, nous indiquons que la fonction se paramètre avec T et F où F sera une fonction retournant un booléen.</p>
<p style="text-align: justify;">Pour voir comment fonctionne ce code c'est <a href="https://is.gd/sXT7Wd">ici</a>.</p>
<p style="text-align: justify;">Voila c'est finit pour cet algorithme, on se revoit la prochaine fois pour un autre algorithme.</p>
<p style="text-align: right;">-- Mathieu</p>
