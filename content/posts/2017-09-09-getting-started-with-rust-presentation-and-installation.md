+++
title = "Getting started with Rust: Presentation and installation"
date = 2017-09-09T00:00:00Z
template = "page.html"
[extra]
comments = true
+++
<p style="text-align: justify;">Hello, today we are going to talk about Rust.</p>
<!-- more -->

<h2 style="text-align: justify;">"You are speaking about it quite often, aren't you?"</h2>
<p style="text-align: justify;">Indeed, more reason to introduce it properly, right?</p>

<h2 style="text-align: justify;">"OK, we are listening."</h2>
<p style="text-align: justify;">First thing first. Rust is a compiled programming language. Which means that the code you write will be translated to another one that the computer actually understand. This translation will be called executable or library (or, more generically, the artifact). Rust is also a strongly typed language. In other words, variable are of a certain type. We'll get back to it another time. Finally, on the <a href="https://www.rust-lang.org/en-US/">official site</a>, Rust is described as:</p>

<blockquote><b>Rust</b> is a systems programming language that runs blazingly fast, prevents segfaults, and guarantees thread safety.</blockquote>
<h2 style="text-align: justify;">"Segfaults? Threads safety?"</h2>
<p style="text-align: justify;">In case you have never practised any "low-level" language such as <strong>C</strong> or <strong>C++</strong>, those terms are certainly new for you. I'll try to rapidly define them here, but we will speak more in detail in some other posts.</p>
<p style="text-align: justify;">Segfault, or segmentation fault, is when a program tries to access memory on which it has no right. Just like at your work, you got a desk to work on but you try to use the one of your colleague, and of the same as your colleague doesn't like that, the computer doesn't either. And then the program crash.</p>
<p style="text-align: justify;">And threads... We also call them "lightweight process". They can work in parallel of others. You can think of it as a pile of dishes to wash. You can use more people to wash all the dishes. The risk is in the case where two or more persons try to wash the same plate at the same time.</p>
<p style="text-align: justify;">Rust prevent those issues and you'll see how in the course of these articles.</p>

<h2 style="text-align: justify;">"OK, but it is still gloomy...."</h2>
<p style="text-align: justify;">Nothing more normal, those notions are quite difficult to apprehend, and it will come in time. So don't worry, be happy.</p>

<h2 style="text-align: justify;">"Fine, do we start?"</h2>
<p style="text-align: justify;">Of course! And the first thing we are going to do is to install Rust. To do so, we are going to use the tool <a href="https://rustup.rs/">Rustup</a>. You will need a terminal from this point on.</p>
<p style="text-align: justify;">If you are using Linux or Mac, this command should be enough:</p>

<pre class="EnlighterJSRAW" data-enlighter-language="shell">curl https://sh.rustup.rs -sSf | sh</pre>
<p style="text-align: justify;">If you are using Windows, go to the <a href="https://rustup.rs/">site of Rustup</a> to download the utility and follow its instructions.</p>
<p style="text-align: justify;">Now, time to use <strong>Cargo</strong>!</p>

<h2 style="text-align: justify;">"Cargo?"</h2>
<p style="text-align: justify;">It's a tool to manage Rust projects. Go into your favourite directory (not the big one little filth, the one for your projects):</p>
<p style="text-align: justify;">For Linux</p>

<pre class="EnlighterJSRAW" data-enlighter-language="shell">cd ~/workspace</pre>
<p style="text-align: justify;">For Windows</p>

<pre class="EnlighterJSRAW" data-enlighter-language="shell">cd %USERPROFILE%\projects</pre>
<p style="text-align: justify;">And then</p>

<pre class="EnlighterJSRAW" data-enlighter-language="shell">cargo new hello_world --bin
cd hello_world</pre>
<p style="text-align: justify;">The flag <code class="EnlighterJSRAW" data-enlighter-language="null">--bin</code>tells to Cargo to make an executable. Without this option, the command will create the configuration for a library.</p>
<p style="text-align: justify;">Inside the folder newly created, you can see a directory named <code class="EnlighterJSRAW" data-enlighter-language="shell">src</code>which will hold up our sources, and a file named <code class="EnlighterJSRAW" data-enlighter-language="shell">Cargo.toml</code>which contains project information:</p>

<pre class="EnlighterJSRAW" data-enlighter-language="ini">[package]
name = "hello_world"
version = "0.1.0"
authors = ["Mathieu Nivoliez"]

[dependencies]
</pre>
<ul style="text-align: justify;">
 	<li>Name</li>
 	<li>Version</li>
 	<li>Author</li>
 	<li>Dependencies</li>
</ul>
<p style="text-align: justify;">Now, take a look to the file <code class="EnlighterJSRAW" data-enlighter-language="shell">main.rs</code>inside <code class="EnlighterJSRAW" data-enlighter-language="shell">src</code>:</p>

<pre class="EnlighterJSRAW" data-enlighter-language="rust">fn main() {
    println!("Hello, world!");
}
</pre>
<h2 style="text-align: justify;">"How are we supposed to read that?"</h2>
<p style="text-align: justify;">I'll help you, do not worry. First, we got a function <code class="EnlighterJSRAW" data-enlighter-language="rust">fn</code> called <code class="EnlighterJSRAW" data-enlighter-language="rust">main()</code>. When called, it will execute the macro <code class="EnlighterJSRAW" data-enlighter-language="rust">println!</code>with <code class="EnlighterJSRAW" data-enlighter-language="rust">"Hello, world!"</code> as parameters, which will print "Hello, world!" in the console.</p>
<p style="text-align: justify;">To compile and execute the program, run <code class="EnlighterJSRAW" data-enlighter-language="shell">cargo run</code>.</p>
<p style="text-align: justify;">Function and macro are big topics, so we will be talking about them later.</p>
<p style="text-align: justify;">In the end, it doesn't .... sorry, in the end we have installed Rust and create a hello world project using Cargo.</p>
<p style="text-align: justify;">See you later in another post on how to getting started with Rust.</p>
<p style="text-align: right;">-- Mathieu</p>
Sources: <a href="https://doc.rust-lang.org/book/second-edition/ch01-00-introduction.html">The "Book"</a>
