+++
title = "DEEP BLOG v2 is here, and hell, there is a lot of work left!"
date = 2017-11-30T18:00:00Z
template = "page.html"
[extra]
comments = true
+++
Hello everyone! Today I would like to talk about this new version of the blog and his future.
<!-- more -->

It's been a while now since I last wrote something. There are lots of reasons but I will only point out this one: the deep blog is now in V2.

# "Does it matter?"

I'm glad you f\*ucking ask!

Here's a non complete list of what it changes:
* The blog no longer use wordpress anymore. I am now using [Cobalt](http://cobalt-org.github.io/), a static site generator written in Rust.
* No more tracking, no google etc. Except for disqus. Should I add one?
* I'm doing the theme and feature of the blog myself. That's why there are very few features.
* I will mostly write in english since it's the english version that is commonly read.
* All the code of the blog is on [my gitlab](https://gitlab.deep-nope.me/mnivoliez/deep-blog).

# "Ok, and for us?"

I wanted also more interaction with you people. About the style of the blog, his shape, his content. So I thought, why not make it easier to talk about all that. So, everyone can make issue on the gitlab. So go one :)

-- Mathieu
