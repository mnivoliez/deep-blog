+++
title = "Créer un jeu avec Amethyst (part 2)"
date = 2017-01-10T00:00:00Z
template = "page.html"
[extra]
comments = true
+++
<p style="text-align: justify;">Bonjour!</p>
<p style="text-align: justify;">Aujourd'hui nous allons parler d'<a href="http://entity-systems.wikidot.com/">ECS</a> (Entity Component System) et de comment on l'utilise dans Amethyst.</p>

<h3 style="text-align: justify;">"ECS? Ça se mange?"</h3>
<p style="text-align: justify;">Non mais vous allez savourer. L'idée est assez simple. Tout ce que vous pouvez voir dans un jeu est une entité (entity). Elle est cratérisée par des composants (component) qui ne contiennent qu'une description des entités. Ces composant seront traités, modifiés par des systèmes (system).</p>
<p style="text-align: justify;">On pourrait dire que les entités sont des nombres (un simple identifiant en somme) aux-quelles sont rattachées des composant qui seront des structures (enregistrement) ou registre organisé de données qui ne possède aucune logique. Ces derniers seront utilisés par des systèmes qui possèdent l'ensemble de la logique.</p>

<h3 style="text-align: justify;">"Un exemple?"</h3>
<p style="text-align: justify;">De suite. Imaginons un jeu avec un personnage. Nous pouvons imaginer que le dans le code le joueur est identifier par le nombre 1 et qu'il possède un composant position et un composant velocity:</p>

<pre class="EnlighterJSRAW" data-enlighter-language="rust">use std::collections::HashMap;

let mut velocities = HashMap::new();
let mut positions = HashMap::new();

struct Velocity(f32, f32, f32)

struct Position(f32, f32, f32)

let player = 1;

let mut player_velocity = Velocity(0.0, 0.0, 0.0);
let mut player_position = Position(0.0, 0.0, 0.0);

velocities.insert(player, player_velocity);
positions.insert(player, player_position);

</pre>
<h3 style="text-align: justify;">"Mais..... Tu peux nous expliquez?"</h3>
<p style="text-align: justify;">J'y vient. Alors ici, on crée deux map. Une map est un une sorte de tableau clé valeur. On peux ensuite accéder aux éléments du tableau en lui donnant la clé. Nous définissons ensuite notre joueur. Il sera l'entité "1". Nous définissons ensuite les structures velocity et position du joueur. Nous insérons ensuite les deux structures dans les maps associées en utilisant le joueur comme clé. C'est une des mise en place possible du système de composants et d'entités.</p>

<h3 style="text-align: justify;">"Ok. Mais avec Amethyst?"</h3>
<p style="text-align: justify;">Avec amethyst, on va construire une entité de la manière suivante:</p>

<pre class="EnlighterJSRAW" data-enlighter-language="rust">world.create_now()
  .with(square.clone())
  .with(LocalTransform::default())
  .with(Transform::default())
  .with(PlayerComponent)
  .build();</pre>
<p style="text-align: justify;"> On peut lire "créer maintenant une entité avec le composant square, localTransform et PlayerComponent".</p>
<p style="text-align: justify;">Pour que cela marche, il faudra aussi instancier le jeu avec les systèmes relatif à ces composants. Mais nous verons cela plus tard avec les "states machines" :)</p>
<p style="text-align: right;">--Mathieu</p>
