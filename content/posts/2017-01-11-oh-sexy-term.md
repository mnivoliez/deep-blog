+++
title = "Oh, sexy term <3"
date = 2017-01-11T00:00:00Z
template = "page.html"
[extra]
comments = true
+++
<p style="text-align: justify;">Welcome!</p>
<p style="text-align: justify;">Today, we're gonna speak about sexy!</p>
<p style="text-align: justify;">If, like me, you have an intense use of your terminal ("vim users, I look at you"), then you would like to make it appeal to your eye. To do that, I give you <a href="http://terminal.sexy/">terminal.sexy</a>!</p>
<p style="text-align: justify;"><img class="alignnone size-full wp-image-292" src="https://blog.deep-nope.me/wp-content/uploads/2017/01/terminal_sexy.png" alt="" width="1919" height="901" /></p>
<p style="text-align: justify;">This tool allow you to test different color set and export it to you favorite terminal.</p>
<p style="text-align: justify;">Here an exemple for termite:</p>

<pre class="EnlighterJSRAW" data-enlighter-language="ini">[colors]

# special
foreground      = #f1ebeb
foreground_bold = #f1ebeb
cursor          = #f1ebeb
background      = #272822

# black
color0  = #48483e
color8  = #76715e

# red
color1  = #dc2566
color9  = #fa2772

# green
color2  = #8fc029
color10 = #a7e22e

# yellow
color3  = #d4c96e
color11 = #e7db75

# blue
color4  = #55bcce
color12 = #66d9ee

# magenta
color5  = #9358fe
color13 = #ae82ff

# cyan
color6  = #56b7a5
color14 = #66efd5

# white
color7  = #acada1
color15 = #cfd0c2
</pre>
<p style="text-align: right;"> -- Mathieu</p>
