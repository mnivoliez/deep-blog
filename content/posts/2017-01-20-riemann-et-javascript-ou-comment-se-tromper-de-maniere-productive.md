+++
title = "Riemann et JavaScript, ou comment se tromper de manière productive!"
date = 2017-01-20T00:00:00Z
template = "page.html"
[extra]
comments = true
+++
<p style="text-align: justify;">Bonjour!</p>
<p style="text-align: justify;">Aujourd'hui je veux vous parlez d'une conversation avec un ami (que je vais appeler <strong>Bob</strong> pour protéger son anonymat).</p>
<p style="text-align: justify;">Avant tout, un peu de contexte. Bob et moi même sommes développeurs (de passion et de métier). Depuis 2-3 ans, je développe de plus en plus en JavaScript et, plus généralement, avec des langages fonctionnels (article en route sur le sujet). Bob, lui, n'aime pas trop le JavaScript, du moins à la base. Il préfère les langages fortement typés. Et c'est OK, chacun ses préférences.</p>
<p style="text-align: justify;">Notre conversation commence sur l'exclamation de Bob: "tiens regarde cette horreur du javascript: 1/0 = ∞". Surpris, je me dis "faisons un script pour tester ça":</p>

<pre class="EnlighterJSRAW" data-enlighter-language="js">console.log('1/0 =', 1/0);</pre>
<p style="text-align: justify;">Et là, horreur! Mon ami ne m'avait pas menti:</p>

<pre class="EnlighterJSRAW" data-enlighter-language="shell">node test.js                                                                                                                                                                                                                                  
1/0= Infinity</pre>
<p style="text-align: justify;">Mais qu'est-ce dont que cela? A ce moment, vos connaissances en mathématiques vous rappellent que la division par 0 est impossible. C'est à ce moment que nos réactions entre mon ami et moi-même ont différé. Au lieu de dire "Argh, quelle horreur!" je me suis demandé "Pourquoi? Quels éléments ont amené à ce choix?".
J'ai donc cherché les raisons et, après quelques recherches, je fus récompensé d'une nouvelle connaissance. Certains d'entre vous aurons entendu parlé de Riemann avec ses séries. Eh bien ce monsieur à aussi une sphère, la <a href="https://fr.wikipedia.org/wiki/Sph%C3%A8re_de_Riemann">sphère de Riemann</a>. Pourquoi cet élément mathématique fut intégré au JavaScript? Je ne sais pas. Je suppose que c'est parce que la sphère est</p>

<blockquote>omniprésente en <a title="Géométrie projective" href="https://fr.wikipedia.org/wiki/G%C3%A9om%C3%A9trie_projective">géométrie projective</a> et en <a title="Géométrie algébrique" href="https://fr.wikipedia.org/wiki/G%C3%A9om%C3%A9trie_alg%C3%A9brique">géométrie algébrique</a> comme exemple fondamental d'une <a title="Variété (géométrie)" href="https://fr.wikipedia.org/wiki/Vari%C3%A9t%C3%A9_%28g%C3%A9om%C3%A9trie%29">variété</a> complexe, d'un <a title="Espace projectif" href="https://fr.wikipedia.org/wiki/Espace_projectif">espace projectif</a>, et d'une <a title="Variété algébrique" href="https://fr.wikipedia.org/wiki/Vari%C3%A9t%C3%A9_alg%C3%A9brique">variété algébrique</a></blockquote>
<p style="text-align: justify;">Le point ici n'est pas de dénigrer mon ami, sa réaction aurait pu être la mienne ou celle de n'importe qui d'autre. L'important, c'est de comprendre pourquoi la recherche de la vérité, de la source, du "pourquoi" est importante et pourquoi nous devons la rechercher coûte que coûte. Ne laissons pas nos sentiments ou nos certitudes nous empêcher d'évoluer.</p>
<p style="text-align: right;">--Mathieu</p>
