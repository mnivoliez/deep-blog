+++
title = "Créer un jeu avec Amethyst (part 1)"
date = 2016-11-29T00:00:00Z
template = "page.html"
[extra]
comments = true
+++
<p style="text-align: justify;">Bonjour!</p>
<p style="text-align: justify;">Aujourd'hui on va parler d'un projet de jeu vidéo que je réalise avec ma chère et tendre.</p>

<h2 style="text-align: justify;">"Un jeu? Quel jeu?"</h2>
<p style="text-align: justify;">C'est une bonne question donc voici le projet.</p>
<p style="text-align: justify;">Il s'agit de réaliser un jeu d'environ 20 minutes. Le jeu mettra en scène un personnage (joueur) qui doit faire à ses différents remords en étant guidé par sa conscience (voix off). Afin de faire face à ses remords, le joueur devra parcourir 4 niveaux de 5 minutes chacun qui se conclura sur un boss.</p>
<p style="text-align: justify;">Le jeu sera un jeu 2d (comme Mario) où seul le clavier sera utilisé.</p>
<p style="text-align: justify;">Le nom actuel est <strong>"Absolution"</strong>, si vous avez des idées pour un autre nom, n'hésitez pas!</p>

<h2 style="text-align: justify;">"Pourquoi faire un jeu?"</h2>
<p style="text-align: justify;">Les enjeux sont multiples.</p>
<p style="text-align: justify;">D'abord, c'est fun. C'est complexe, intéressant et pour peu qu'on soit avec le ou les bons partenaires ça peut devenir une très bonne expérience.</p>
<p style="text-align: justify;">C'est également un challenge personnel. Je compte faire du jeu vidéo mon métier dans le futur, et dans cet objectif, je compte entrer dans une école de jeu vidéo. Avoir un jeu avant peu faciliter mon entrée dans une telle école. C'est aussi un challenge vis-à-vis des technologies utilisées.</p>

<h2 style="text-align: justify;">"Mais c'est vrai ça! Vous utilisez quelles techno?"</h2>
<p style="text-align: justify;">Alors le jeu est développer en <a href="https://www.rust-lang.org">Rust</a> en utilisant le moteur <a href="https://github.com/amethyst/amethyst">Amethyst</a>. Pourquoi ces technologies-là? Et bien déjà, Rust, bien que jeune, est un language fonctionnel puissant qui me plait. Il est compilé et donne des performances équivalentes à d'autres languages tel que le c++. Ensuite, Amethyst est un moteur qui met en oeuvre le paradigme <a href="http://entity-systems.wikidot.com/">ECS</a> en s'appuyant sur <a href="https://github.com/slide-rs/specs">SPECS</a>. D'un point de vue graphique, c'est ma partenaire qui s'en occupe. Je ne fais qu'intégrer son travail dans le code.</p>

<h2 style="text-align: justify;">"Bon ok. Et pourquoi en parler sur ton blog?"</h2>
<p style="text-align: justify;">J'en parle car je trouve ça intéressant. Et je vous propose de suivre cette magnifique aventure que ma douce et moi-même vivont, et par la même de découvrir Rust et Amethyst.</p>
<p style="text-align: justify;">Dès qu'un nouvel article sur le sujet sera prêt, je mettrais le lien ici.</p>
<p style="text-align: right;">-- Mathieu</p>
