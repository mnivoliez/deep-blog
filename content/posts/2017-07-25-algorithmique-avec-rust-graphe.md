+++
title = "Algorithmique avec Rust: Graphe"
date = 2017-07-25T00:00:00Z
template = "page.html"
[extra]
comments = true
+++
<p style="text-align: justify;">Bonjour, aujourd’hui on va parler de graphe: ce que c’est et comment le représenter en Rust.</p>
<!-- more -->
<p style="text-align: justify;">Mais avant petite info. Au départ, je pensais parler du tri fusion, toutefois, Vaidehi Joshi à couvert le sujet dans son article <a href="https://dev.to/vaidehijoshi/making-sense-of-merge-sort-part-1">Making sense of merge sort</a>. Je vous conseille de le lire, il est très pertinent.</p>
<p style="text-align: justify;">Revenons-en au sujet: les <strong>GRAPHES</strong>!</p>

<h2 style="text-align: justify;">"OK, c’est quoi un graphe?"</h2>
<p style="text-align: justify;">Pour faire simple, un graphe c’est un ensemble de points, appelés nœuds ou sommets, reliés par des traits, appelés arêtes. Dans certains cas, les arêtes sont orientées, comme des flèches, et dans ce cas le graphe sera dit orienté.</p>


[caption id="" align="aligncenter" width="231"]<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/23/Directed_graph_no_background.svg/231px-Directed_graph_no_background.svg.png" alt="File:Directed graph no background.svg" width="231" height="153" data-file-width="231" data-file-height="153" /> Graphe orienté[/caption]
<p style="text-align: justify;">Dans le cas contraire, le graphe sera non orienté.</p>


[caption id="" align="aligncenter" width="333"]<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/5b/6n-graf.svg/333px-6n-graf.svg.png" alt="File:6n-graf.svg" width="333" height="220" data-file-width="333" data-file-height="220" /> Graphe non orienté[/caption]
<p style="text-align: justify;">Je passe sur les familles de graphes pour le moment, plus d’infos sur la page <a href="https://fr.wikipedia.org/wiki/Th%C3%A9orie_des_graphes">Wikipédia</a> qui en parle très bien.</p>

<h2 style="text-align: justify;">"OK, mais à quoi ça sert?"</h2>
<p style="text-align: justify;">Quel lecteur incroyable, c’est encore une très bonne question!</p>
<p style="text-align: justify;">Et là, je vous réponds avec un scoop! vous les voyez, vous les utilisez et vous ne le savez pas. "Où?" me direz-vous. Les réseaux sociaux, les télécommunications, etc. Même dans la programmation orientée objet vous pouvez voir un graphe (imbrication d’objet).</p>

<h2 style="text-align: justify;">"OK, un graphe c’est utile. Comment qu’on les programme?"</h2>
<p style="text-align: justify;">Comme la série traite du langage Rust, je vais vous montrer <strong>UNE </strong>méthode pour créer une telle structure dans ce langage.</p>
<p style="text-align: justify;">D’abord, nous pouvons résumer notre graphe par le fait qu’il possède un ensemble de nœuds, un ensemble d’arêtes et qu’il peut être orienté.</p>

<pre class="EnlighterJSRAW" data-enlighter-language="rust">struct Graph&lt;'a&gt; {
  nodes: Vec&lt;&amp;'a Node&gt;,
  edges: Vec&lt;Edge&lt;'a&gt;&gt;,
  oriented: bool,
}</pre>
<p style="text-align: justify;">Ensuite, nous devons définir les nœuds et les arêtes.</p>

<pre class="EnlighterJSRAW" data-enlighter-language="rust">struct Node {}

struct Edge&lt;'a&gt; {
  source_node: &amp;'a Node,
  target_node: &amp;'a Node,
}</pre>
<p style="text-align: justify;">Nous avons donc les structures nécessaires pour faire un graphe.</p>
<p style="text-align: justify;">Lien vers l’exemple: <a href="https://play.rust-lang.org/?gist=6a0f7a676b8ae13617e90497d90bb54f&amp;version=stable">lien</a>.</p>
<p style="text-align: justify;">On va s’arrêter là pour aujourd’hui, je ne voudrais pas vous endormir. On se retrouve prochainement pour jouer avec les graphes.</p>
<p style="text-align: right;">-- Mathieu</p>
