+++
title = "L'erreur nécessaire au succès."
date = 2017-09-15T00:00:00Z
template = "page.html"
[extra]
comments = true
+++
<p style="text-align: justify;">Bonjour, aujourd'hui on va parler des erreurs, pourquoi elles sont nécessaires et comment les approcher.</p>
<!-- more -->

<h2 style="text-align: justify;">"Tu vas nous faire nous sentir mal?"</h2>
<p style="text-align: justify;">Pas du tout, j’espère même le contraire.</p>
<p style="text-align: justify;">On recherche tous la perfection dans nos travaux. Nous voulons tous éliminer les erreurs et les rendre les plus parfaits possible. Et souvent, nous oublions une règle simple: les erreurs sont des carburants de la création. Certains d'entre vous suivent peut-être la chaîne YouTube <a href="https://www.youtube.com/channel/UCO1cgjhGzsSYb1rsB4bFe4Q">FunFunFunction</a> animée par <a href="https://twitter.com/mpjme">@MPJ</a>. Dans un <a href="https://www.youtube.com/watch?v=6YBV1cKRqzU&amp;t=1484s">épisode récent</a>, il décrit comment il laisserait consciemment de la duplication pour laisser émerger un modèle. Comme il le dit lui même, la duplication est mal et devrait être évitée. Mais les erreurs devraient être gérées avec raison. Ce qui m’amène à plusieurs points.</p>

<h3 style="text-align: justify;">Erreurs il y a, erreur il y aura toujours.</h3>
<p style="text-align: justify;">Les erreurs arriveront, vous n'êtes pas une machine, et donc, vous n'êtes pas parfait (et heureusement, ça serait monotone). Donc ne pas reconnaître les erreurs ne fera que les rendre invisibles et les rendra difficiles à gérer.</p>

<h3 style="text-align: justify;">Les erreurs font de vous quelqu'un de meilleur.</h3>
<p style="text-align: justify;">On apprend rarement de nos succès. En revanche, l'erreur est le meilleur enseignant qui soit. En faisant des erreurs, vous vous forcez à trouver de nouvelles voies pour gérer un problème, vous exercez votre réflexion à travers une difficulté.</p>

<h2 style="text-align: justify;">"Que devrait-on faire alors?"</h2>
<p style="text-align: justify;">Bonne question. Mon conseil est d'accepter les erreurs et de prévoir un cadre où l'on peut les gérer. Les tests sont un moyen, les revues à plusieurs en sont un autre. Également, il faut éviter la recherche prématurée de la perfection. Je m'explique: beaucoup d'organisation demande une planification en amont, par exemple des diagrammes UML avant le développement d'un programme, et souvent, cela ne marche pas. Le but de cette planification est de pouvoir obtenir un résultat parfait en un coup. Et je peux vous assurer que cela n'arrivera pas. Beaucoup d'éléments s'y opposeront, la fatigue, un outil obsolète, un manque de temps, un imprévu, etc.</p>
<p style="text-align: justify;">En quelques mots, laisser de la place pour les erreurs. Et pour les entreprises: apprenez à ne pas sanctionner les erreurs. Elles devraient être une occasion d'apprendre et non de punir. Bien entendu, c'est à adapter en fonction de la situation, mais vous avez l'idée.</p>
<p style="text-align: justify;">C'est tout ce que j'ai à dire pour le moment. Cet article a été compliqué à écrire pour moi et je ne suis pas sûr de m'être exprimé correctement. Mais le sujet me tient à cœur: je vois beaucoup de personnes se rendre folles pour une simple erreur et être bloquées par "les failles d'une conception parfaite", à tel point que je veux mettre ce problème en avant. Si vous avez des commentaires, conseils ou histoire qui entrent dans le sujet, lâchez-vous dans les commentaires.</p>
<p style="text-align: right;">-- Mathieu</p>
