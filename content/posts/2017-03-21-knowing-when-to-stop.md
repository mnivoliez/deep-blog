+++
title = "Knowing when to stop"
date = 2017-03-21T00:00:00Z
template = "page.html"
[extra]
comments = true
+++
Hello! Today I would like to give you feedback about Amethyst and why I have stopped using it for our <a href="http://blog.deep-nope.me/en/2017/01/11/create-a-game-with-amethyst-part-1/">game</a>.
<!-- more -->
<h2>"You gave up Amethyst?"</h2>
Yep, and to understand why, let me set up the context a little. I love functionnal programming and data driven development. Therefor, Amethyst is an engine based on an ECS (Entities Components Systems) which is strongly answering to my liking. Moreover the engine is new, the community is active....
<h2>"But????"</h2>
There are several problems! First, it's too new so it's not stable yet. Breaking changes are popping out every week if not every day leading me to rewrite part of code more frequently than I would like. Secondly, it's 3d oriented while our game is in 2d. Naturally, it has became hard and painful to develop with it.
<h2>"Could you have seen it from the start?"</h2>
Obviously yes... But I was blinded by my feelings and that is the whole point of my post! We love our code. We love the tools we use to do it. We will stick to a technology we like and reject the one we dislike. And often we do it without actually asking our self if the technology is really appropriate. We are scared to admit it after start. I was blind to Amethyst flaws because I love the idea of Amethyst. It made the developpement hard and painful. I finished to change when I noticed that I wasn't enjoying it anymore because of the flaws. So I think we should admit our feelings toward technologies, not to reject them but to be aware of them and not be controlled by them.

It's my feedback. Your opinion?
