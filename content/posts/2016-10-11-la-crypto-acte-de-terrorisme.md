+++
title = "La crypto, acte de terrorisme?"
date = 2016-10-11T00:00:00Z
template = "page.html"
[extra]
comments = true
+++
<p style="text-align: justify;">Aujourd'hui, comme tous les matins, je regarde les news. Et je tombe sur cet <span style="text-decoration: underline;"><a href="http://www.zdnet.com/article/london-police-charge-man-with-terrorism-over-use-of-encryption/">article</a></span> sur une affaire au Royaume Uni. Rien de bien affolant pour le moment. Jusqu’à ce que je voie</p>

<blockquote>
<p style="text-align: justify;">Specifically, Ullah was charged with a count of preparing to engage in an act of terrorism namely by "researching an encryption program, developing an encrypted version of his blog site and publishing the instructions around the use of program on his blog site."</p>
</blockquote>
<p style="text-align: justify;">En résumé, une des accusations contre monsieur Samata Ullah est d'utiliser le chiffrement sur son site et de rechercher des outils de chiffrement de données.</p>
<p style="text-align: justify;">Rapide rappel: le chiffrer c'est modifier une information (par exemple un texte) avec une méthode (par exemple en interchangeant des lettres: le "a" devient le "c", etc) afin de le rendre illisible tant qu'il n'est pas le re-modifier à l'état d'origine.</p>
<p style="text-align: justify;">L'article en rajoute ensuite une couche:</p>

<blockquote>"Section 5 can criminalize acts that, on their own, would be completely legal -- if prosecutors can show that the end purpose of those acts might be terrorism," said Tayab Ali, a human rights lawyer, <a href="http://www.vice.com/en_uk/read/katie-engelhart-interview-tayab-ali-anti-terror-legislation-830" target="_blank">speaking to Vice News</a> in 2014.</blockquote>
<p style="text-align: justify;">Cela signifie que si un acte légal <span id="spans0e0">a</span> pour finalité de manière directe ou indirecte le terrorisme, l'acte devient coupable. Pour caricaturer un peu la chose, si vous <span id="spans1e0">vouliez</span> faire un attentat à la bombe et que vous achetez <span id="spans1e1">des croissants</span> le matin parce que vous <span id="spans1e2">aimez</span> les croissants, l'achat du croissant peut devenir <span id="spans1e3">illégal</span>.</p>
<p style="text-align: justify;">Ayant un doute sur l'absurdité de la chose, j'ai <span id="spans0e0">regardé</span> d'autres sources. <span id="spans1e0">À</span> tout <span id="spans1e1" class="sac">hasard</span>, <span style="text-decoration: underline;"><a href="http://www.publicnow.com/view/9357326D6E2FCF7B0BFFA839C6B4F210BD0AF3C5">Public Now</a></span> et <span style="text-decoration: underline;"><a href="https://www.theguardian.com/uk-news/2016/oct/04/man-arrested-on-cardiff-street-to-face-six-terror-charges">The Guardian</a></span> qui confirment la chose. Ce qui est grave ici, <span id="spans2e0">ce n'est pas</span> le fait qu'un terroriste soit arrêté, bien au contraire. Le problème c'est que le chiffrement est un outil et le fait de l'utiliser ne constitue pas en <span id="spans3e0">soi</span> un acte terroriste ou alors je suis un terroriste du <span id="spans3e1" class="msac">ssh</span> (Hail SSH!). Il est grave que l'on puisse étendre le soupçon à ce point. Mon activité <span id="spans4e0">pourrait</span> devenir illégale par ce que je suis suspecté de terrorisme?</p>
<p class="watch-title-container" style="text-align: justify;">Pour rappel, le chiffrement des données est essentiel pour beaucoup de personnes et d'entreprises. Pour ceux qui ne s'en <span id="spans5e0">rendent</span> pas compte, je vais utiliser la métaphore de John Oliver de <span id="spans5e1" class="msac">LastWeekTonight</span> lors de l'épisode <span id="spans5e2" class="sac">Gouvernement</span> <span id="spans5e3">surveillance</span>:</p>
[embed]https://www.youtube.com/watch?v=XEVlyP4_11M[/embed]
<p style="text-align: justify;">Si vous voulez envoyer une image de votre pénis à quelqu'un, vous préférez (en général) que PERSONNE d'autre que le destinataire ne le voit. Vous ne posteriez pas cette image sur Instagram. Le chiffrement permet de faire en sorte que lors de l'envoi de votre photo, personne d'autre que le destinataire ne puisse accéder à l'image.</p>
<p style="text-align: justify;">Alors on peut se dire:</p>

<h3 style="text-align: justify;">"Oui mais c'est au Royaume Uni, ça ne nous concerne pas."</h3>
<p style="text-align: justify;">Et j'aimerai dire que oui. Mais, malheureusement, l'UE n'a visiblement pas notre vie privée dans ses priorités comme le montre cet <span style="text-decoration: underline;"><a href="https://www.laquadrature.net/fr/directive-terrorisme-parlement-europeen-cede-lachement-sirenes-securitaires">article</a></span>.</p>
<p style="text-align: justify;">Bref on n'est pas sorti de l'auberge.</p>
<p style="text-align: right;">-- Mathieu</p>
<p style="text-align: justify;"><strong>BONUS: </strong>pour ceux qui veulent en savoir plus sur l'informatique, le terrorisme, etc:</p>
[embed]https://www.youtube.com/watch?v=Ed9GTZSL3zU[/embed]
