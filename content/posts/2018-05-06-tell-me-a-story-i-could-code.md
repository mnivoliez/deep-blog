+++
title = "Tell me a story I could code"
date = 2018-05-06T17:52:52Z
template = "page.html"
draft = false
[extra]
comments = true
+++
Hello everyone! Today one colleague and friend of mine, Gaetan, would like to propose you a simple (or not) concept!

It's a code of which you are the story teller!
<!-- more -->

## "Whaaaaat?"
![whaaaaat](http://i0.kym-cdn.com/photos/images/newsfeed/000/768/057/7e2.gif)

Let me explain! We propose you to tell a story (a little one), sentence by sentence. Each time a sentences is selected, we will try to express the story into code.

An exemple is worth 1000 speech so here is the start of our story:

### "Once upon a time, a young knight name Bob found a penny!"

```rust
#[derive(Debug)]
struct Story {
    pub time: &'static str,
    pub main_character: Character,
}

#[derive(Debug)]
struct Character {
    pub name: &'static str,
    pub money: i32,
}

fn found_a_strange_penny(character: &mut Character) {
    character.money += 1;
    println!("Oh a penny! \" said {}.\"", character.name);
}

fn main() {
    let mut story = Story {
        time: "Once upon a time",
        main_character: Character {
            name: "Bob",
            money: 0,
        },
    };

    found_a_strange_penny(&mut story.main_character);
}
```
All the code will live in this [repository](https://gitlab.deep-nope.me/mnivoliez/tell-me-story-i-could-code).

So, it's up to you, reader, to continue the story.
Post a comment with one sentence or two and we will try to code that.
We will choose the comment that appeal us the most!

Start :p

--
Gaetan and Mathieu
