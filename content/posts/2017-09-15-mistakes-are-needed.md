+++
title = "Mistakes are needed!"
date = 2017-09-15T00:00:00Z
template = "page.html"
[extra]
comments = true
+++
<p style="text-align: justify;">Hello, today we are going to speak about mistakes, why they are needed and how to approach it.</p>
<!-- more -->

<h2 style="text-align: justify;">‘Are you going to make us feel bad?’</h2>
<p style="text-align: justify;">Not at all. Quite the opposite in fact.</p>
<p style="text-align: justify;">We all endeavour for the better in our project. We want to wipe out mistakes, we want to make it as perfect as it is possible to. And often, we forgot one simple rule. Mistake is fuel to creation. Some of you may follow the YouTube channel <a href="https://www.youtube.com/channel/UCO1cgjhGzsSYb1rsB4bFe4Q">FunFunFunction</a> animated by <a href="https://twitter.com/mpjme">@MPJ</a>. In a relatively <a href="https://www.youtube.com/watch?v=6YBV1cKRqzU&amp;t=1484s">new video</a>, he spoke about how he should have left duplication in his code to let a pattern emerge. As he said himself, duplication is bad and should be avoided. But any mistake should be taken care of in a reasonable way. Thus, it leads me to several points I would like to rise.</p>

<h3 style="text-align: justify;">Mistakes there are and mistakes will always be.</h3>
<p style="text-align: justify;">Mistakes are bound to happen. You’re not a machine, and therefore, you are not perfect (hopefully, it would be boring). Denying it will only make mistakes less visible and difficult to manage.</p>

<h3 style="text-align: justify;">Mistakes will make you better.</h3>
<p style="text-align: justify;">Sadly, we rarely learn from success. On the other end, failure teach us a lot. By making mistakes, you force yourself to find new ways to deal with a problem, you exercise your reflection over a difficulty.</p>

<h2 style="text-align: justify;">‘So, what should we do?’</h2>
<p style="text-align: justify;">Here the crafty question. My best advice is to accept mistakes and set up an environment to able to deal with them. Testing is one way, review is another one, and there are many more. Also, lower your initial standard. Let me explain, many organisations require upfront conception, for example UML diagram before programming, and often, it doesn’t work. The goal is to make it perfect in one shot. To get something working at the first try. I assure you, it will most likely never happen, you will work with obsolete tool, you will lack some knowledge about something, a coworker will be less accurate, etc.</p>
<p style="text-align: justify;">So, in four words, leave room for mistakes. They are your safeguard to creation. And to companies, learn not to reprove mistakes. A mistake should be an occasion to learn, not to punish. Of course, this should be adapted in function of the situation, but you got the idea.</p>
<p style="text-align: justify;">That’s all I have to say for now. This article was quite difficult to write for me and I am not sure to have expressed my mind correctly. But, it’s close to my heart: I see so many people being mad about not making mistakes and being stuck by ‘flaws of a perfect design’ and I want to highlight this issue. If you have any comment, advice or stories about this, please tell them.</p>
<p style="text-align: right;">—Mathieu</p>
