+++
title = "Ouverture de la billetterie pour le HellFest 2017"
date = 2016-10-06T00:00:00Z
template = "page.html"
[extra]
comments = true
+++
<p style="text-align: justify;">Ça y est! Les billets pour le HellFest 2017 sont disponibles!!! Comme tous les ans, ils vont disparaître vite.</p>
<p style="text-align: justify;">En 2016, le line-up était exceptionnel: Rammstein, Twisted Sisters, Black Sabbath, Amon Amarth, MegaDeth (pour n'en citer que quelques uns), ce qui interroge sur le prochain line-up. Traditionnellement, la direction du HellFest dévoile le programme avant les fêtes. Il va falloir patienter!
Ben Barbaud nous tease tous de même en disant:</p>

<blockquote>Des têtes d'affiches reconnues, des valeurs montantes, des nouveaux en qui nous croyons et bien sur des ovnis inqualifiables !</blockquote>
<p style="text-align: justify;">et une programmation</p>

<blockquote>renouvelée à 40 %</blockquote>
<p style="text-align: justify;">(source <a href="http://www.infoconcert.com/news/festival--hellfest-2017--le-festival-metal-le-plus-celebre-de-france-met-en-vente-ses-pass-3-jours-aujourdhui--11406.html">Info Concert</a>)</p>


<hr />
<p style="text-align: justify;">Également, certains changements sont à prévoir. Notamment l’agrandissement de l'espace devant les main stages comme le souligne Alexx Rebecq, le directeur de la communication du festival:</p>

<blockquote><em>Le grand changement cette année, c'est que l'<strong>on va agrandir l'espace devant les main stage</strong>. On va rajouter quasiment un terrain de foot devant, sans augmenter la jauge public, afin de redonner du confort à nos festivaliers, ce qui est une priorité pour nous</em></blockquote>
<p style="text-align: justify;">(source <a href="http://www.rtl2.fr/actu/regions/metallica-n-ira-pas-au-hellfest-en-2017-7785136845">RTL2</a>)</p>


<hr />
<p style="text-align: justify;">Bref, que du bon!</p>
<p style="text-align: right;">-- Mathieu</p>
