+++
title = "L'éducation autrement?"
date = 2016-11-03T00:00:00Z
template = "page.html"
[extra]
comments = true
+++
<p style="text-align: justify;">Bonjour!</p>
<p style="text-align: justify;">Si comme moi vous vous intéressez à ce genre de sujet, vous avez dû entendre parler des écoles alternatives, ou encore des écoles Finlandaises, etc...</p>
<p style="text-align: justify;">Ces écoles sont innovantes dans leur manière de transmettre le savoir. Souvent en ayant une approche plus "communautaire" de l'apprentissage, comme ces écoles des pays nordiques où les élèves sont amenés à apprendre à chercher des informations eux-mêmes afin de résoudre des problématique.</p>
<p style="text-align: justify;">Hélas, le constat en France n'est clairement pas aussi élogieux. Bien que des écoles alternatives voient le jour, l’école traditionnelle domine toujours l’éducation de nos jeunes têtes. Ces écoles pour la plupart ne me semblent pas adaptées. Pour me baser sur mon expérience, je ne trouve pas que l'école fut un endroit "cool" pour moi (je parle jusqu'au lycée). On y va, on travaille un truc sans application, on nous note pour évaluer notre valeur (ou comment dire "en histoire tu vaux 8") et à la fin on obtient éventuellement un papier qui dit "bravo, tu as une valeur sur papier". Je regrette profondément que l'éducation ait échoué à m'intéresser à certains sujets. J'aurais aimé qu'on me parle histoire en essayant de me faire comprendre le pourquoi du comment (<a href="https://www.youtube.com/user/notabenemovies">#notabene</a>), qu'on m'explique la géographie en mettant en avant les relations politico-commerciales vis-à-vis de l’actualité, qu'on me dise pas que l'informatique n’était pas intéressante parce que ce n’était pas au bac. J'aurais aimé aussi qu'on m'explique comment gérer la pression au lieu de me l'imposer, qu'on m'apprenne à vivre sainement, etc....</p>
<p style="text-align: justify;">Mais notre éducation ne reflète pas cela. Notre éducation est plus dans un sentiment de "comment faire des gens aptes pour notre société". Leur bonheur? Qui s'en préoccupe, leur santé? On a des médicaments. La chose terrible c'est qu'on est dans un système qui explique concrètement que si les élèves sont malheureux dans leurs études ils iront voir l'assistante sociale.</p>
<p style="text-align: justify;">Et puis récemment, je suis tombé sur cette vidéo:</p>
[embed]https://www.youtube.com/watch?time_continue=600&v=-FliEaIn7CA[/embed]
<blockquote>"Oh sacre bleu!!! Un gosse qui va pas à l'école, mais ses parents sont dingues!!</blockquote>
<p style="text-align: justify;">C'est ce que vont dire la majorité des parents. On peut les comprendre, ils s'inquiètent pour l'avenir de leurs enfants. Les parents veulent que leurs enfants aient un bon travail, qu'ils gagnent bien leur vie, qu'ils se marient, qu'ils aient des enfants eux aussi.</p>
<p style="text-align: justify;">Comme dirait l'enfant de cette vidéo:</p>

<blockquote>It's how you'll be happy, right?</blockquote>
<p style="text-align: justify;">("C'est comme ça qu'on devient heureux, hein?")</p>
<p style="text-align: justify;">J'en suis pas sûr. Personnellement, je suis heureux en partant en voyage, en faisant de l'informatique du matin au soir, en écoutant du heavy metal, en parlant avec mes grands-parents, en m'incluant dans des projets communautaires, en essayant de manger mieux, en trouvant la paix, etc... L'école ne m'a rien appris de tout ça. Ce que je fais dans la vie ne me vient pas de l'école. J'apprends à mieux écrire depuis que je tiens ce blog parce que j'ai envie de faire un travail de qualité. Pas parce que ma prof de français m'a donné 7 en dictée.</p>
<p style="text-align: justify;">Eh bien que des initiatives louables soient mise en place (projet interdisciplinaire) les ressources, elles, n'y sont pas. Ce que je veux dire ici c'est qu'il existe d'autres solutions. Que chacun peut réfléchir à d'autres voies, que nous pouvons écouter plus (parents - enfants) et que les clefs de succès ne devraient pas être décidé sur juste une note. Je ne dis pas que les notes sont le mal, mais qu'elles ne sont pas tout. Malheureusement, aujourd'hui si vous demandez à un enfant/ado ce qu'il pense de lui vis-à-vis de ses notes, le constat risque d'être différent. Nous n'apprenons pas à gérer notre vie, nous apprenons à faire des équations du 2/3eme degrés sans voir où ça nous mène. Peut-être vaudrait-il mieux transformer notre éducation, le mettre plus en phase avec la réalité. Un de mes professeur (Mr Brunetto) a dit une chose véridique:</p>

<blockquote>En entreprise, il n'y a pas de note. Ça marche, ou ça marche pas.</blockquote>
<p style="text-align: justify;">Pour finir, je vous laisse sur une vidéo de LastWeekTonight sur la question des notes:</p>
[embed]https://www.youtube.com/watch?v=J6lyURyVz7k[/embed]
<p style="text-align: right;">-- Mathieu</p>
