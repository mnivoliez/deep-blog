+++
title = "Algorithmics with Rust: Graph"
date = 2017-07-25T00:00:00Z
template = "page.html"
[extra]
comments = true
+++
<p style="text-align: justify;">Hello, today we are going to talk about graphs, what are they and how do we make them in Rust.</p>
<!-- more -->
<p style="text-align: justify;">Before that, I was planning of speaking about merge sort. But, Vaidehi Joshi has already covered the subject in her article <a href="https://dev.to/vaidehijoshi/making-sense-of-merge-sort-part-1">Making sense of merge sort</a>. You should really read it for it is well written and really easy to understand.</p>
<p style="text-align: justify;">Back to subject: <strong>GRAPHS</strong>!</p>

<h2 style="text-align: justify;">"OK, what is a graph?"</h2>
<p style="text-align: justify;">To keep it simple, a graph is a collection of points, called nodes, and a collection of edges linking nodes. Graph can be directed, where edges will be like arrows, or undirected.</p>


[caption id="" align="alignleft" width="231"]<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/23/Directed_graph_no_background.svg/231px-Directed_graph_no_background.svg.png" alt="File:Directed graph no background.svg" width="231" height="153" data-file-width="231" data-file-height="153" /> Directed graph[/caption]

[caption id="" align="aligncenter" width="333"]<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/5b/6n-graf.svg/333px-6n-graf.svg.png" alt="File:6n-graf.svg" width="333" height="220" data-file-width="333" data-file-height="220" /> Undirected graph[/caption]
<p style="text-align: justify;">Graph can be divided in class, buuuuuut I let you seek it out on <a href="https://en.wikipedia.org/wiki/Graph_(discrete_mathematics)#Important_classes_of_graph">Wikipedia</a>, the article is well documented.</p>

<h2 style="text-align: justify;">"OK, what does it use for?"</h2>
<p style="text-align: justify;">Good question, our reader is a good one for sure!</p>
<p style="text-align: justify;">What I will say will stun you: you see them every day! In telecommunication, social media, etc. Even in programming, for example in object oriented language, you can see and use graph.</p>

<h2 style="text-align: justify;">"OK, it's useful. How do we make them?"</h2>
<p style="text-align: justify;">I'll show you <strong>ONE</strong> method to make them in Rust.</p>
<p style="text-align: justify;">First, we can define a graph by a collection of nodes, a collection of edges and by being directed or not.</p>

<pre class="EnlighterJSRAW" data-enlighter-language="rust">struct Graph&lt;'a&gt; {
  nodes: Vec&lt;&amp;'a Node&gt;,
  edges: Vec&lt;Edge&lt;'a&gt;&gt;,
  directed: bool,
}</pre>
<p style="text-align: justify;">Then, we define node and edge.</p>

<pre class="EnlighterJSRAW" data-enlighter-language="rust">struct Node {}

struct Edge&lt;'a&gt; {
  source_node: &amp;'a Node,
  target_node: &amp;'a Node,
}</pre>
<p style="text-align: justify;">Now we have all needed structures to make graphs.</p>
<p style="text-align: justify;">You can see an example here: <a href="https://play.rust-lang.org/?gist=6a0f7a676b8ae13617e90497d90bb54f&amp;version=stable">link</a>.</p>
<p style="text-align: justify;">We will stop here for today, I don't want to bore you. See you next time to play around graph.</p>
<p style="text-align: right;">-- Mathieu</p>
