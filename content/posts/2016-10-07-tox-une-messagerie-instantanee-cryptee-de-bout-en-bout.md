+++
title = "Tox, une messagerie instantanée cryptée de bout en bout"
date = 2016-10-07T00:00:00Z
template = "page.html"
[extra]
comments = true
+++
<p style="text-align: justify;">La communication c'est important. La sécurité aussi c'est important. Avoir les deux c'est obligatoire pour beaucoup d'entreprises et sensible pour de plus en plus de gens. C'est là qu'intervient <a href="https://tox.chat/index.html">Tox</a>. C'est une messagerie instanée chiffrée de bout en bout <span id="spans1e1">c'est-à-dire</span> que seul vous et votre correspondant <span id="spans1e2">pouvez</span> voir votre conversation. Il va même plus loin en proposant un système décentralisé: il n'y a pas de serveur "central" ou transit vos données. La communication à votre destinataire est directe.</p>

<h4 style="text-align: justify;">"<span id="spans2e0">Ça</span> a l'air cool ton truc, où puis-je le trouver?"</h4>
<p style="text-align: justify;">Bonne question. <span id="spans2e0">Ça</span> dépend de votre plateforme.</p>

<ul style="text-align: justify;">
 	<li>Si vous êtes sous Windows, c'est par là: <a href="https://tox.chat/download.html">téléchargement</a>.</li>
 	<li>Si vous êtes sous Linux: il est certainement dans les dépots de votre package manager.</li>
 	<li>Si vous êtes sous Mac, regardez le lien précédent ou regardez sous homebrew (peut-être).</li>
</ul>
<h4 style="text-align: justify;">"Je l'ai téléchargé mais comment ça s'utilise?"</h4>
<p style="text-align: justify;">D'abord ouvrez Tox, il devrait ouvrir une fenêtre comme cela:</p>
<img class="alignnone size-full wp-image-111" src="http://blog.deep-nope.me/wp-content/uploads/2016/10/qtox_login.png" alt="qtox_login" width="412" height="218" />
<p style="text-align: justify;">Entrez un nom d'utilisateur et un mot de passe. Puis la fenêtre suivant devrait apparaitre:</p>
<img class="alignnone size-full wp-image-112" src="http://blog.deep-nope.me/wp-content/uploads/2016/10/qtox_standard_screen.png" alt="qtox_standard_screen" width="958" height="1013" />
<p style="text-align: justify;">Pour ajouter un ami, appuyez sur la croix en bas à gauche et vous l'écran d'ajout apparaîtra:</p>
<img class="alignnone size-full wp-image-113" src="http://blog.deep-nope.me/wp-content/uploads/2016/10/qtox_add_screen.png" alt="qtox_add_screen" width="956" height="1013" />
<p style="text-align: justify;">Pour ajouter un ami, il suffira d'entrer son ID dans le champ correspondant.</p>

<h4 style="text-align: justify;">"Mais où peut on trouver cet ID?"</h4>
<p style="text-align: justify;">Vous le trouverez en cliquant sur votre nom d'utilisateur en haut à gauche:</p>
<img class="alignnone size-full wp-image-114" src="http://blog.deep-nope.me/wp-content/uploads/2016/10/qtox_ID.png" alt="qtox_id" width="962" height="1015" />
<p style="text-align: justify;">Et voilà! Vous pouvez maintenant communiquer de manière sécurisée!</p>
<p style="text-align: right;">-- Mathieu</p>
