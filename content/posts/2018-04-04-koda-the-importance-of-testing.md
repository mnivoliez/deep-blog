+++
title = "Koda: the importance of testing"
date = 2018-04-04T15:09:17Z
template = "page.html"
draft = false
[extra]
comments = true
+++
Hello everyone! Today we are going to talk about the importance of testing in a game project.
<!-- more -->

## "Are you going to speak about Koda?"
That’s right, I will speak about our experience in developing *Koda Kiyomori’s guardian*.
![Koda Title](/images/KodaTitle.png)

I will scope a little the subject, I will speak about play test or testing in real condition.
## "What do you mean by test in real condition?"
What I call test in real condition is when you test how well is received, or how behave, the thing you are building into an environment close or identical to the production one.
In our case, we develop a game, so we put people to test it.

## "How did you do?"
That’s actually a really great question. We got the chance to have a team member who had worked inside a community center and we were able to go there to put our game to the test with the kids.
To do that, we established a ‘test protocol’.
1. Children answered a first survey revealing their sensibility toward video games.
2. We observed children playing the game, noting their behaviors and reactions to the game.
3. Children took a second survey to get their direct individual feedback.
4. We organized discussion with children to get deeper into what was globally well received or not.
![Play test](/images/playtest_koda.jpg)

## "Was it successful?"
Yes, it was. We were able to determine several bugs and flaws in our game design. And we validated that the global direction of the game was the right one.

More specifically, we got several feedback about the camera behavior and the difficulty of the different zones of the game.

And everybody loved our mascot Koda :p
![Koda](/images/koda.png)

## "So it was perfect?"
No, absolutely not! First we were very stressed. We had never done that before. Here the biggest elements we could (and will) improve for next time:
* The survey design made it difficult to fill it quickly, we should maybe have used something like Google form from the start instead of word document.
* We roughly knew the number of children, we should have known the exact number and established test groups beforehand instead of doing it on the fly.
* We planned to record test sessions but the camera was not well centred, so many record was difficult to exploit afterwards.

There is certainly a pile of elements I forgot here, but I think it is a pretty good list already.

## "Will you do it again?"
Yes, actually, we organise play test on a daily basis to enhance our game. And as for play test at the scale I described, we already have a new appointment with the kids. :)

Overall, making tests like that is really important to at least *validate* what you are building. The sooner you do it, the sooner you will know if your idea works or not.
In our case, we significantly improved gameplay and mechanics. We also, and that is really important, gain more confidence in our project. It helps us to trust in our project and in our capacity to deliver something at the level of our expectations. And that is the crux of everything, if your team is confident and motivate, you will have better chance of success.

Before you leave this post, I got some news! The team *Koda* will be present at the [Japan Touch](http://japantouch-haru.com/2018/) in Lyon the 7th and 8th of April, so if you are there, come test our game! More on our [Twitter](https://twitter.com/Kodamagora) and our [Facebook](https://www.facebook.com/Kodamagora/)!

-- Mathieu
