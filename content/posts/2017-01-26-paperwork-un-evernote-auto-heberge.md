+++
title = "Paperwork, un Evernote auto-hébergé."
date = 2017-01-26T00:00:00Z
template = "page.html"
[extra]
comments = true
+++
<p style="text-align: justify;"><strong>Attention! Cet article s'adresse aux gens qui connaissent linux, la ligne de commande, mysql, php et nginx.</strong></p>
<!-- more -->
<p style="text-align: justify;">Bonjour!</p>
<p style="text-align: justify;">Aujourd'hui on va parler de <a href="http://paperwork.rocks/">Paperwork</a>!</p>

<h2 style="text-align: justify;">"Mais qu'est-ce donc?"</h2>
<p style="text-align: justify;">Alors, <a href="http://paperwork.rocks/">Paperwork</a> est un outil de gestion de note auto hébergé. En d'autres termes, un <a href="https://evernote.com/">Evernote</a> chez soit.</p>

<h2 style="text-align: justify;">"Ok, comment on l'installe?"</h2>
<p style="text-align: justify;">Pour installer <a href="http://paperwork.rocks/">Paperwork</a>, ouvrez une invite de commande et entrez les commande suivantes en <strong>root</strong>:</p>

<ol style="text-align: justify;">
 	<li>Installez php et mysql:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">// ubuntu
apt-get update
apt-get install mysql-server php5-mysql  nginx php5-fpm curl wget git php5-cli php5-gd php5-mcrypt nodejs</pre>
</li>
 	<li>Procédez à la configuration initiale de mysql:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">/usr/bin/mysql_secure_installation</pre>
</li>
 	<li>Activez mcrypt dans php en modifiant les fichiers <code class="EnlighterJSRAW" data-enlighter-language="null">/etc/php5/fpm/php.ini</code> et <code class="EnlighterJSRAW" data-enlighter-language="shell">/etc/php5/cli/php.ini</code>:
<pre class="EnlighterJSRAW" data-enlighter-language="ini">extension=mcrypt.so</pre>
</li>
 	<li>Installez composer:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer</pre>
</li>
 	<li>Rendez-vous dans le dossier <code class="EnlighterJSRAW" data-enlighter-language="null">/var/www</code> où nous allons mettre <a href="http://paperwork.rocks/">Paperwork</a>:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">cd /var/www/</pre>
</li>
 	<li>Clonez [Paperwork](http://paperwork.rocks/) et entrez dans le dossier:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">git clone https://github.com/twostairs/paperwork.git
 cd ./paperwork/frontend/</pre>
</li>
 	<li>Entrez dans <code class="EnlighterJSRAW" data-enlighter-language="shell">mysql</code> et ajoutez un nouvel utilisateur avec sa base pour <a href="http://paperwork.rocks/">Paperwork</a>:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">mysql -u root -p</pre>
<pre class="EnlighterJSRAW" data-enlighter-language="sql">DROP DATABASE IF EXISTS paperwork;
CREATE DATABASE IF NOT EXISTS paperwork DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
GRANT ALL PRIVILEGES ON paperwork.* TO 'paperwork'@'localhost' IDENTIFIED BY 'paperwork' WITH GRANT OPTION;
FLUSH PRIVILEGES;
quit</pre>
</li>
 	<li>Copiez la configuration de base pour que <a href="http://paperwork.rocks/">Paperwork</a> se connecte à la base de donnée:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">cp app/storage/config/default_database.json app/storage/config/database.json</pre>
</li>
 	<li>Utilisez le script fournit par <a href="http://paperwork.rocks/">Paperwork</a> pour remplir la base:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">php artisan migrate</pre>
</li>
 	<li>Donnez les droit à www-data pour le dossier de <a href="http://paperwork.rocks/">Paperwork</a> (attention, sur certains systèmes c'est l'utilisateur http):
<pre class="EnlighterJSRAW" data-enlighter-language="null">chown www-[extra]www-data -R /var/www/</pre>
</li>
 	<li>Ajoutez une configuration pour nginx:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">vim /etc/nginx/sites-available/paperwork.domain.com</pre>
<pre class="EnlighterJSRAW" data-enlighter-language="ini">server {
        listen   80;
        # listen 443 ssl;

        root /var/www/paperwork/frontend/public;
        index index.php index.html index.htm;

        server_name example.com;

        # server_name example.com;
        # ssl_certificate /etc/nginx/ssl/server.crt;
        # ssl_certificate_key /etc/nginx/ssl/server.key;

        location / {
                try_files $uri $uri/ /index.php;
        }

        error_page 404 /404.html;

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
              root /usr/share/nginx/www;
        }

        # pass the PHP scripts to FastCGI server listening on the php-fpm socket
        location ~ \.php$ {
                try_files $uri =404;
                fastcgi_pass unix:/var/run/php5-fpm.sock;
                fastcgi_index index.php;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                include fastcgi_params;

        }

}
</pre>
<pre class="EnlighterJSRAW" data-enlighter-language="shell">ln -s /etc/nginx/sites-avalaible/paperwork.domain.com /etc/nginx/sites-enable/paperwork.domain.com
</pre>
</li>
 	<li> Ajoutez un fichier <code class="EnlighterJSRAW" data-enlighter-language="shell">setup</code> pour évitez que la configuration de <a href="http://paperwork.rocks/">Paperwork</a> échoue:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">sudo -u www-data touch app/storage/config/setup</pre>
</li>
 	<li> Installez nodejs:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">curl -sL https://deb.nodesource.com/setup_7.x |  bash -
apt-get install -y nodejs
</pre>
</li>
 	<li>installez <code class="EnlighterJSRAW" data-enlighter-language="shell">bower</code> et <code class="EnlighterJSRAW" data-enlighter-language="shell">gulp</code> globalement:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">npm install -g gulp bower</pre>
</li>
 	<li> Installez les dépendances de <a href="http://paperwork.rocks/">Paperwork</a>:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">sudo -u www-data npm install
sudo -u www-data bower install
sudo -u www-data gulp</pre>
</li>
 	<li>Redémarrez nginx et php:
<pre class="EnlighterJSRAW" data-enlighter-language="shell">service nginx restart
service php5-fpm restart</pre>
</li>
</ol>
<p style="text-align: justify;">Et voilà! Vous avez un <a href="http://paperwork.rocks/">Paperwork</a> fonctionnel sur le port 80.</p>
<p style="text-align: right;">-- Mathieu</p>
