+++
title = "Hello, world"
date = 2016-10-05T00:00:00Z
template = "page.html"
[extra]
comments = true
+++
<pre class="EnlighterJSRAW" data-enlighter-language="rust">fn main() { 
println("Hello, world"); 
}</pre>
<p style="text-align: justify;">C'est la tradition. Et ce blog ne coupe pas à la règle.</p>
<p style="text-align: justify;">Il y a plusieurs sujets dont je veux parler, en permanence. De la programmation au système en passant par de la conception et du DevOps, de la politique à l'écologie en passant par du DIY... Sans jamais le faire. Et finalement il est temps de se lancer!</p>


<hr />
<p style="text-align: right;">-- Mathieu</p>
