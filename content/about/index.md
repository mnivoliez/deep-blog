+++
title = "Hello, I am Mathieu NIVOLIEZ"
template = "page.html"
[extra]
comments = true
+++
I love programming! Especially game programming and Rust, but I also do other stuff!
So I thought it could be good to tell the world about it. Since then, I write this blog, so feel free to leave a comment or share this blog!

## I am looking for a job in game or graphic programming!

I am currently looking for a full time job starting in October. It could be in France, abroad or even remote.

You will find my resume just here: [resume.pdf](resume.pdf)

Contact me at [pro@mathieu-nivoliez.com](mailto:pro@mathieu-nivoliez.com) or through social network: <a href="https://github.com/mnivoliez"><i class="fab fa-github-square"></i></a>     <a href="https://gitlab.deep-nope.me/mnivoliez"><i class="fab fa-gitlab"></i></a>     <a href="https://www.linkedin.com/in/mnivoliez"><i class="fab fa-linkedin"></i></a>     <a href="https://twitter.com/MNivoliez"><i class="fab fa-twitter-square"></i></a>     <a href="https://www.facebook.com/mathieu.nivoliez"><i class="fab fa-facebook-square"></i></a> 

> All language sucks, but some like Rust, sucks a little less - Guillaume Bouchard